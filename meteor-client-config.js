module.exports = {
  runtime: {
    DDP_DEFAULT_CONNECTION_URL: process.env.DDP_DEFAULT_CONNECTION_URL || 'http://localhost:3000',
    ROOT_URL: process.env.ROOT_URL || 'http://localhost:3000',
    autoupdate: {
      versions: {},
    },
  },
  generateNodeModules: true,
  import: [
    'meteor-base',
    'mobile-experience',
    'mongo',
    'reactive-var',
    'tracker',
    'standard-minifier-css',
    'standard-minifier-js',
    'es5-shim',
    'ecmascript',
    'accounts-password',
    'session',
    'http',
    'react-meteor-data',
    'alanning:roles',
    'ostrio:files',
  ],
  npmPackages: [
    'react',
    'react-dom',
  ],
};
