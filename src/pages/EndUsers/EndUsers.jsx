import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Button, Tag, Card, Icon,
} from 'antd';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import EndUsersCollection from '../../api/EndUsers';
import { searchTables } from '../../components/tablesFilters';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

const removeUser = (user) => {
  Modal.confirm({
    iconType: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove ${user.emails[0].address} ?`,
    content: 'Would you actually want to remove this user!.',
    onOk() {
      Meteor.call('users.methods.remove', {
        userId: user._id,
      }, (err, res) => {
        if (err) {
          message.error('something went wrong');
        } else {
          message.success('User removed successfully');
        }
      });
    },
    onCancel() {

    },
  });
};

class EndUsers extends Component {
  constructor(props) {
    super(props);
    this.state = { searchText: '' };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
    document.title = 'Endusers | Crowdaid';
  }

  render() {
    const {
      users, pagination, loading, history,
    } = this.props;
    const dataSource = users.map(user => ({
      ...user,
      key: user._id,
      email: user.emails[0].address,
    }));

    const columns = [
      {
        title: 'Username',
        dataIndex: 'username',
        key: 'username',
      },
      {
        title: 'Email Address',
        dataIndex: 'email',
        key: 'email',
        ...this.getColumnSearchProps('email', 'Emails', this),
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div className="end-users__actions">
            {record.profile
              ? (
                <Button
                  type="primary"
                  shape="circle-outline"
                  title="view & edit Profile"
                  icon="solution"
                  onClick={() => history.push(`/users/${record._id}`)}
                />
              )
              : <Tag color="cyan">No profile yet</Tag>
            }
            <Button
              type="danger"
              shape="circle-outline"
              title="Delete"
              icon="delete"
              onClick={() => removeUser(record)}
            />
          </div>
        ),
      },
    ];
    return (
      <div className="container">
        <Card
          bordered={false}
          title={(
            <h3>
              <Icon type="user" />
              {' '}
               End Users
            </h3>
          )}
        >
          <Table
            pagination={pagination}
            loading={loading}
            dataSource={dataSource}
            columns={columns}
          />
        </Card>
      </div>);
  }
}

EndUsers.propTypes = {
  loading: PropTypes.bool.isRequired,
  users: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const usersHandle = Meteor.subscribe('EndUsersTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!usersHandle.ready()) {
    return ({
      loading: true,
      users: [],
      pagination: {},
    });
  }
  const usersMeta = CollectionsMeta.findOne({ _id: 'EndUsers' });

  const users = EndUsersCollection.find({}, {
    sort: { createdAt: -1 },
  });
  const totalUsers = usersMeta && usersMeta.count;
  // usersMeta.count;
  const usersExist = totalUsers > 0;
  return {
    loading: false,
    usersReady: usersHandle.ready(),
    users: usersExist ? users.fetch() : [],
    total: totalUsers,
    pagination: {
      position: 'bottom',
      total: totalUsers,
      current: PageNumber.get(),
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(EndUsers);
