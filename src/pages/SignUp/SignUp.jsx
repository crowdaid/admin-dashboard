import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { Accounts } from 'meteor/accounts-base';
import { message, Spin } from 'antd';
import { validateNationalNum } from '../../components/utils';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = { user: null, loading: true };
    // eslint-disable-next-line react/prop-types
    const { history } = this.props;
    // set the title of the page
    document.title = 'Create Account | CrowdAid';
    // redirect if the user already logged in
    if (Meteor.userId()) history.replace('/');
  }

  componentDidMount() {
    const { match: { params: { token } } } = this.props;
    Meteor.call('users.methods.get_user_token', { token }, (err, user) => {
      if (user) {
        this.setState({ user, loading: false });
      } else {
        this.setState({ loading: false });
      }
    });
  }

  // reset user fullname and password
  handleSubmit(e) {
    e.preventDefault();
    const firstName = e.target.firstName.value.trim();
    const lastName = e.target.lastName.value.trim();
    const mobile = e.target.mobile.value;
    const idNumber = e.target.idNumber.value;
    const password = e.target.password1.value;
    const password2 = e.target.password2.value;
    // eslint-disable-next-line react/prop-types
    const { match: { params: { token } } } = this.props;
    // validation on password
    if (password.length < 6) {
      message.warning('Password should be at least 6 characters ');
      return;
    } if (password !== password2) {
      message.warning('Passwords is not match');
      return;
    }
    if (!firstName || !lastName || !mobile || !idNumber) {
      message.warning('All fields are required.');
      return;
    }
    // validate the national number
    if (!validateNationalNum(idNumber)) {
      message.warning('Please add a valid national ID number');
      return;
    }
    // eslint-disable-next-line react/prop-types
    const { history } = this.props;
    // reset user password
    Meteor.call('users.methods.register_admin', {
      firstName, lastName, mobile, idNumber, token,
    }, (error, res) => {
      if (error) message.error(error.reason);
      else {
        // reset password on the client
        Accounts.resetPassword(token, password, (err) => {
          if (err) message.error(`Something went wrong, ${err.reason}`);
          else history.replace('/');
        });
      }
    });
  }

  render() {
    const { user, loading } = this.state;
    if (loading) return <Spin className="register" size="large" />;
    return (
      !user ? (
        <div className="register">
          <div className="register__wrapper">
            <h3>This token is expired</h3>
            <p>please contact the administrator</p>
          </div>
        </div>
      )
        : (
          <div className="register">
            <div className="register__wrapper">
              <div className="register__title">Create admin account</div>
              <p className="register__info">Complete your information</p>
              <p className="register__info">{user && user.emails[0].address}</p>
              <form className="register__form" onSubmit={this.handleSubmit}>
                <input required type="text" name="firstName" placeholder="Enter your first name" />
                <input required type="text" name="lastName" placeholder="Enter your last name" />
                <input required type="text" name="mobile" placeholder="Enter your mobile number" />
                <input required type="text" name="idNumber" placeholder="Enter your ID number" />
                <input required type="password" name="password1" placeholder="Enter your password" />
                <input required type="password" name="password2" placeholder="Confirm your password" />
                <button type="submit" className="register__btn">Sign me up</button>
              </form>
            </div>
          </div>
        )
    );
  }
}

export default withRouter(SignUp);
