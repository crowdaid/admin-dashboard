import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Icon, Tag, message, Card,
} from 'antd';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import DonationsCollection from '../../api/Donations';
// table filters and sorting prams
import { searchTables } from '../../components/tablesFilters';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

// mark the donation as collected
class UnallocatedDonations extends Component {
  constructor(props) {
    super(props);
    // set the title of the page
    document.title = 'Unallocated Donations | CrowdAid ';
    this.state = { searchText: '' };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
    this.acceptOffer = this.acceptOffer.bind(this);
  }

  acceptOffer(donation) {
    const { history } = this.props;
    Meteor.call('donations.methods.accept_offer', { donation }, (err, res) => {
      if (err) {
        message.info(err.reason);
      } else {
        message.success('Donation collected successfully');
      }
    });
  }

  render() {
    const {
      dons, pagination, loading,
    } = this.props;
    const dataSource = dons.map(don => ({
      _id: don._id,
      quantity: don.quantity,
      donationMethod: don.donationMethod || '-',
      collectionAddress: don.collectionAddress || '-',
      collectionTime: (don.collectionTime && don.collectionTime.date.toLocaleDateString()) || '-',
      phone: don.phone || '-',
      createdAt: don.createdAt,
      userId: don.userId || 'فاعل خير',
      key: don._id,
    }));

    const columns = [
      {
        title: 'Donor',
        dataIndex: 'userId',
        key: 'userId',
        ...this.getColumnSearchProps('userId', 'users', this),
      },
      {
        title: 'Money quantity',
        dataIndex: 'quantity',
        key: 'quantity',
        sorter: (a, b) => a.quantity - b.quantity,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Donation method',
        dataIndex: 'donationMethod',
        key: 'donationMethod',
      },
      {
        title: 'Collection address',
        dataIndex: 'collectionAddress',
        key: 'collectionAddress',
        ...this.getColumnSearchProps('collectionAddress', 'Addresses', this),

      },
      {
        title: 'Mobile number',
        dataIndex: 'phone',
        key: 'phone',
        ...this.getColumnSearchProps('phone', 'Mobile numbers', this),
      },
      {
        title: 'Collection time',
        dataIndex: 'collectionTime',
        key: 'collectionTime',
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (<Tag color="blue"> Soon </Tag>),
      },
    ];

    return (
      <div className="container">
        <Card
          bordered={false}
          title={(
            <h3>
              <Icon type="appstore" />
              {' '}
              Unallocated Donations
            </h3>
          )}
        >
          <Table
            pagination={pagination}
            loading={loading}
            dataSource={dataSource}
            columns={columns}
          />
        </Card>
      </div>
    );
  }
}

UnallocatedDonations.propTypes = {
  loading: PropTypes.bool.isRequired,
  dons: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const donsHandle = Meteor.subscribe('DonationsTabular', {
    filter: { status: 'Unallocated' },
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
    setOrgId: false,
  });
  if (!donsHandle.ready()) {
    return ({
      loading: true,
      dons: [],
      pagination: {},
    });
  }

  const donsMeta = CollectionsMeta.findOne({ _id: 'Donations' });

  const dons = DonationsCollection.find({ orgId: null, status: 'Unallocated' }, {
    sort: { createdAt: -1 },
  });
  const totalDons = donsMeta && donsMeta.count;

  const donsExist = totalDons > 0;

  return {
    loading: false,
    dons: donsExist ? dons.fetch() : [],
    total: totalDons,
    pagination: {
      position: 'bottom',
      total: totalDons,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(withRouter(UnallocatedDonations));
