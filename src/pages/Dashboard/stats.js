import { Meteor } from 'meteor/meteor';
import Cases from '../../api/Cases';
import Needs from '../../api/Needs';
import Beneficiaries from '../../api/Beneficiaries';
import Donations from '../../api/Donations';
import EndUsersCollection from '../../api/EndUsers';
import OrgsCollection from '../../api/Organizations';
import CategoriesCollection from '../../api/Categories';
import AdminsCollection from '../../api/Admins';

const needStatuses = ['untouched', 'touched', 'fulfilled'];
const caseStatuses = ['draft', 'approved', 'pending', 'rejected', 'fulfilled', 'terminated'];
const urgencyFlags = ['immediate', 'today', 'thisWeek', 'thisMonth', 'thisYear', 'N/A'];
const verificationStatuses = ['pending', 'verified', 'moreInfoNeeded', 'rejected'];
const beneficiaryTypes = ['individual', 'animal', 'group', 'family', 'organization', 'place'];
const donationMethods = ['Cash on door', 'Credit card', 'NoMoney'];
const donationStatuses = ['Pending', 'Collected', 'Failed', 'Cancelled'];
const donationType = ['money', 'items', 'noquantity'];

// calculate cases statistics
export const generateCasesData = () => ({
  total: {
    lable: 'Total Cases',
    value: Cases.find({ }).count(),
  },
  urgencyData: {
    datasets: [{
      data: urgencyFlags.map(urgency => Cases.find({ urgency }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', '#f1c40f',
      ],
    }],
    labels: urgencyFlags,
  },
  statusData: {
    datasets: [{
      data: caseStatuses.map(status => Cases.find({ status }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', '#f1c40f',
      ],
    }],
    labels: caseStatuses,
  },
});


// calculate needs statistics
export const generateNeedsData = () => ({
  total: {
    lable: 'Total Needs',
    value: Needs.find({ }).count(),
  },
  urgencyData: {
    datasets: [{
      data: urgencyFlags.map(urgency => Needs.find({ urgency }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', '#f1c40f',
      ],
    }],
    labels: urgencyFlags,
  },
  statusData: {
    datasets: [{
      data: needStatuses.map(status => Needs.find({ status }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db',
      ],
    }],
    labels: needStatuses,
  },
});

// calculate Beneficiaries statistics
export const generateBeneficiariesData = () => ({
  total: {
    lable: 'Total Beneficiaries',
    value: Beneficiaries.find({ }).count(),
  },
  typesData: {
    datasets: [{
      data: beneficiaryTypes
        .map(type => Beneficiaries
          .find({ type }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6', '#e74c3c', '#f1c40f',
      ],
    }],
    labels: beneficiaryTypes,
  },
  statusData: {
    datasets: [{
      data: verificationStatuses
        .map(verificationStatus => Beneficiaries
          .find({ verificationStatus }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#e74c3c',
      ],
    }],
    labels: verificationStatuses,
  },
});

// calculate Donations statistics
export const generateDonationsData = () => ({
  total: {
    lable: 'Total Donations',
    value: Donations.find({ }).count(),
  },
  methodsData: {
    datasets: [{
      data: donationMethods
        .map(donationMethod => Donations
          .find({ donationMethod }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db',
      ],
    }],
    labels: donationMethods,
  },
  statusData: {
    datasets: [{
      data: donationStatuses.map(status => Donations.find({ status }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db', '#9b59b6',
      ],
    }],
    labels: donationStatuses,
  },
  typesData: {
    datasets: [{
      data: donationType.map(type => Donations.find({ type }).count()),
      backgroundColor: [
        '#4b6584', '#2ecc71', '#3498db',
      ],
    }],
    labels: donationType,
  },
});

export const generateDonorsData = () => ({
  total: {
    lable: 'Total Donors',
    value: EndUsersCollection.find({}).count(),
  },
});

export const generateAdminsData = () => ({
  total: {
    lable: 'Total Admins',
    value: AdminsCollection.find({}).count(),
  },
});
export const generateCategoriesData = () => ({
  total: {
    lable: 'Total Categories',
    value: CategoriesCollection.find({}).count(),
  },
});
export const generateOrgsData = () => ({
  total: {
    lable: 'Total Organizations',
    value: OrgsCollection.find({}).count(),
  },
});

let statsData = null;
Meteor.call('users.methods.get_admin_dahsboard_stats', { type: 'admin' }, (err, res) => {
  if (!err) statsData = res;
});

export const generateLineData = () => ({
  data: {
    datasets: [{ // cases line data
      data: statsData && statsData[0].map(casee => ({ x: casee.date, y: casee.count })),
      label: 'Cases',
      backgroundColor: '#3e95cd',
      borderColor: '#3e95cd',
      fill: false,
    }, { // needs line data
      data: statsData && statsData[1].map(need => ({ x: need.date, y: need.count })),
      label: 'Needs',
      backgroundColor: '#2ecc71',
      borderColor: '#2ecc71',
      fill: false,
    }, { // doners line data
      data: statsData && statsData[2].map(don => ({ x: don.date, y: don.count })),
      label: 'Donors',
      backgroundColor: '#9b59b6',
      borderColor: '#9b59b6',
      fill: false,
    }],
  },
  options: {
    scales: {
      xAxes: [{
        type: 'time',
        ticks: { source: 'data' },
        time: {
          displayFormats: {
            quarter: 'MMM YYYY',
          },
        },
      }],
    },
    title: {
      display: true,
      text: 'Cases, Needs and Donors comparison ber month',
    },
    animation: {
      duration: 0, // general animation time
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    hover: {
      mode: 'nearest',
      intersect: true,
      animationDuration: 0, // duration of animations when hovering an item
    },
    responsiveAnimationDuration: 0, // animation duration after a resize
  },
});

export const generateDonatainsBarData = () => ({
  data: {
    datasets: [{
      data: statsData && statsData[3].map(don => ({ x: don.date, y: don.count })),
      label: 'Donations',
      backgroundColor: '#9b59b6',
      borderColor: '#9b59b6',
    }],
  },
  options: {
    scales: {
      xAxes: [{
        type: 'time',
        ticks: { source: 'data' },
        time: {
          unit: 'month',
          displayFormats: {
            quarter: 'MMM YYYY',
          },
        },
      }],
    },
    tooltips: {
      mode: 'index',
      intersect: false,
    },
    hover: {
      mode: 'nearest',
      intersect: true,
      animationDuration: 0, // duration of animations when hovering an item
    },
    title: {
      display: true,
      text: 'Donations ber month',
    },
    barPercentage: 1,
    animation: {
      duration: 0, // general animation time
    },
    responsiveAnimationDuration: 0, // animation duration after a resize
  },
});
