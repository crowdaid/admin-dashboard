import React from 'react';
import {
  Card, Row, Col, Icon,
} from 'antd';
import Statistic from 'antd/lib/statistic';
import {
  Pie, Doughnut, Polar, Line, Bar,
} from 'react-chartjs-2';
import PropTypes from 'prop-types';

const Dashboard = ({
  totalstats,
  totalstats: {
    cases, needs, bens, donations, lineData, donationsBarData,
  }, ready,
}) => (
  <div className="dashboard">
    <h1>
      <Icon type="dashboard" />
      {' '}
        Dashboard
    </h1>
    <div className="dashboard__totals">
      { ready && Object.keys(totalstats).map(stat => (
        totalstats[stat].total
        && (
        <div key={totalstats[stat].total.lable} className="dashboard__totals__card">
          <Statistic
            title={(
              <div>
                <Icon type="area-chart" />
                {'  '}
                {totalstats[stat].total.lable}
              </div>
            )}
            valueStyle={{ fontWeight: 'bold', fontSize: 30 }}
            value={totalstats[stat].total.value}
          />
        </div>
        )
      ))}
    </div>
    <Row gutter={8} type="flex">
      <Col span={24} className="dashboard__col">
        <Card bordered={false} loading={!ready}>
          {ready && lineData && (
          <Line
            data={lineData && lineData.data}
            options={lineData.options}
          />
          )}
        </Card>
      </Col>
    </Row>
    <Row gutter={8} type="flex">
      <Col span={8} className="dashboard__col">
        <Card
          loading={!ready}
          title={(
            <div className="dashboard__card-title">
              <img
                style={{ borderRadius: '50%' }}
                src="/images/cases.png"
                width="29"
                height="29"
                alt="crowd aid"
              />
              <b> Cases </b>
            </div>
          )}
          className="dashboard__card"
        >
          <div className="dashboard__card__info">
            <div className="dashboard__card__info__number">
              {cases && (
              <Pie
                height={200}
                options={{ title: { display: true, text: 'Urgency', position: 'top' } }}
                data={cases.urgencyData}
                legend={{ position: 'left', fullWidth: false }}
              />
              )}
              {cases && (
              <Doughnut
                height={200}
                legend={{ position: 'left', fullWidth: false }}
                options={{ title: { display: true, text: 'Status', position: 'top' } }}
                data={cases.statusData}
              />
              )}
            </div>
          </div>
        </Card>
      </Col>
      <Col span={8} className="dashboard__col">
        <Card
          loading={!ready}
          title={(
            <div className="dashboard__card-title">
              <img src="/images/needs.png" width="25" height="25" alt="crowd aid" />
              <b>Needs</b>
            </div>
          )}
          className="dashboard__card"
        >
          <div className="dashboard__card__info">
            <div className="dashboard__card__info__number">
              {needs && (
              <Pie
                height={200}
                options={{ title: { display: true, text: 'Urgency', position: 'top' } }}
                data={needs.urgencyData}
                legend={{ position: 'left', fullWidth: false }}
              />
              )}
              {needs && (
              <Doughnut
                height={200}
                legend={{ position: 'left', fullWidth: false }}
                options={{ title: { display: true, text: 'Status', position: 'top' } }}
                data={needs.statusData}
              />
              )}
            </div>
          </div>
        </Card>
      </Col>
      <Col span={8} className="dashboard__col">
        <Card
          loading={!ready}
          className="dashboard__card"
          title={(
            <div className="dashboard__card-title">
              <img src="/images/hand.png" width="25" height="25" alt="crowd aid" />
              <b>Beneficiaries</b>
            </div>
          )}
        >
          <div className="dashboard__card__info">
            <div className="dashboard__card__info__number">
              {bens && (
              <Pie
                height={200}
                options={{ title: { display: true, text: 'Type', position: 'top' } }}
                data={bens.typesData}
                legend={{ position: 'left', fullWidth: false }}
              />
              )}
              {bens && (
              <Doughnut
                height={200}
                legend={{ position: 'left', fullWidth: false }}
                options={{ title: { display: true, text: 'Status', position: 'top' } }}
                data={bens.statusData}
              />
              )}
            </div>
          </div>
        </Card>
      </Col>
    </Row>
    <Row gutter={8} type="flex">
      <Col span={24} className="dashboard__col">
        <Card
          loading={!ready}
          className="dashboard__card"
          title={(
            <div className="dashboard__card-title">
              <img src="/images/donations.png" width="30" height="30" alt="crowd aid" />
              <b>Donations </b>
            </div>
          )}
        >
          <div className="dashboard__card__info">
            <div className="flex-donation">
              <div>
                {donations && (
                <Pie
                  height={200}
                  options={{ title: { display: true, text: 'Type', position: 'top' } }}
                  data={donations.typesData}
                  legend={{ position: 'left', fullWidth: false }}
                />
                )}
                {donations && (
                <Polar
                  height={200}
                  options={{ title: { display: true, text: 'Pay Methods ', position: 'top' } }}
                  data={donations.methodsData}
                  legend={{ position: 'left', fullWidth: false }}
                />
                )}
              </div>
              <div className="dashboard__mid-doughut">
                {donations && (
                <Doughnut
                  height={200}
                  legend={{ position: 'left', fullWidth: false }}
                  options={{ title: { display: true, text: 'Status', position: 'top' } }}
                  data={donations.statusData}
                />
                )}
              </div>
              <div>
                {donations && donationsBarData
                  && (
                  <Bar
                    height={430}
                    legend={{ display: false }}
                    data={donationsBarData.data}
                    options={donationsBarData.options}
                  />
                  )
                  }
              </div>
            </div>
          </div>
        </Card>
      </Col>
    </Row>
  </div>
);


Dashboard.propTypes = {
  ready: PropTypes.bool.isRequired,
  totalstats: PropTypes.instanceOf(Object).isRequired,
};

export default Dashboard;
