import './Dashboard.css';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Dashboard from './Dashboard';
import {
  generateCasesData, generateDonationsData, generateDonatainsBarData,
  generateNeedsData, generateBeneficiariesData, generateLineData,
  generateCategoriesData, generateDonorsData, generateOrgsData,
  generateAdminsData,
} from './stats';

export default withTracker((props) => {
  document.title = 'Dashboard | CrowdAid';
  const statsHandle = Meteor.subscribe('Statistics');
  if (!statsHandle.ready()) {
    return {
      ready: false,
      totalstats: {},
    };
  }
  // cases statistics
  const cases = generateCasesData();
  const needs = generateNeedsData();
  const bens = generateBeneficiariesData();
  const donations = generateDonationsData();
  const admins = generateAdminsData();
  const endUsers = generateDonorsData();
  const categories = generateCategoriesData();
  const orgs = generateOrgsData();
  const lineData = generateLineData();
  const donationsBarData = generateDonatainsBarData();

  const totalstats = {
    admins,
    endUsers,
    orgs,
    categories,
    cases,
    needs,
    bens,
    donations,
    lineData,
    donationsBarData,
  };
  return {
    ready: true,
    totalstats,
  };
})(Dashboard);
