import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Spin, Button, Input, Tag, Icon, Card, Form,
} from 'antd';

import { CollectionsMeta } from '../../api/CollectionsMeta';
import Admins from '../../api/Admins';
import { isAdmin, isSuperAdmin, validateEmail } from '../../components/utils';
import { searchTables } from '../../components/tablesFilters';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

const removeAdmin = (user) => {
  Modal.confirm({
    iconType: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove admin role for ${user.emails[0].address} ?`,
    content: 'This user will no longer have admin privileges.',
    onOk() {
      Meteor.call('users.methods.remove', {
        userId: user._id,
      }, (err, res) => {
        if (err) {
          message.error('something went wrong');
        } else {
          message.success('Admin removed successfully');
        }
      });
    },
    onCancel() {

    },
  });
};

class AdminsPage extends Component {
  constructor(props) {
    super(props);
    this.state = { searchText: '', sending: false };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
    // create reference to the invite email input
    this.inviteEmailRef = React.createRef();
    // bind admin functions with Admins class
    this.inviteAdmin = this.inviteAdmin.bind(this);
    // set the title of the page
    document.title = 'Admins | Crowdaid';
  }

  inviteAdmin(e) {
    e.preventDefault();
    this.inviteEmailRef.current.focus();
    const inviteEmail = this.inviteEmailRef.current.input.value;
    // validate email
    if (!validateEmail(inviteEmail)) {
      message.error('The Email must be valid, please check it!');
      return;
    }
    this.setState({ sending: true });
    // invite the admin by email
    Meteor.call('users.methods.send_invitation', { email: inviteEmail, role: 'admin' }, (err, res) => {
      this.setState({ sending: false });
      if (err) message.error('Some thing went wrong, check the email address and try again!');
      else message.info(res);
    });
  }


  render() {
    const {
      currentUser, users, pagination, loading, history,
    } = this.props;
    const { sending } = this.state;
    const dataSource = users.map((user) => {
      if (!user.profile) user.profile = [];
      return {
        ...user,
        key: user._id,
        first_name: user.profile.firstName || '-',
        last_name: user.profile.lastName || '-',
        mobile: user.profile.mobile || '-',
        email: user.emails[0].address || '-',
        identification: user.profile.idNumber || '-',
      };
    });
    const columns = [
      {
        title: 'First name',
        dataIndex: 'first_name',
        key: 'first_name',
        ...this.getColumnSearchProps('first_name', 'first name', this),
      },
      {
        title: 'Last name',
        dataIndex: 'last_name',
        key: 'last_name',
        ...this.getColumnSearchProps('last_name', 'last name', this),
      },
      {
        title: 'Mobile',
        dataIndex: 'mobile',
        key: 'mobile',
        ...this.getColumnSearchProps('mobile', 'mobiles', this),
      },
      {
        title: 'ID',
        dataIndex: 'identification',
        key: 'identification',
        ...this.getColumnSearchProps('identification', 'IDs', this),

      },
      {
        title: 'Email Address',
        dataIndex: 'email',
        key: 'email',
        ...this.getColumnSearchProps('email', 'Emails', this),
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => {
          const isCurrentUser = currentUser && record._id === currentUser._id;
          const recordIsSuperAdmin = isSuperAdmin(record);
          if (isCurrentUser || recordIsSuperAdmin) {
            return (
              <span>
                {isCurrentUser && (
                <Tag color="#87d068"> You </Tag>
                )}
                {recordIsSuperAdmin && (
                <Tag color="#f50"> SuperAdmin </Tag>
                )}
              </span>
            );
          }
          return (

            <div className="admins__actions">
              {isSuperAdmin(Meteor.userId()) && (record.profile.length !== 0
                ? (
                  <Button
                    type="primary"
                    shape="circle-outline"
                    title="view & edit Profile"
                    icon="solution"
                    onClick={() => history.push(`/users/${record._id}`)}
                  />
                )
                : <Tag color="cyan">No profile yet</Tag>)
              }
              {isAdmin(record)
                && (
                <Button
                  type="danger"
                  shape="circle-outline"
                  title="Delete"
                  icon="delete"
                  onClick={() => removeAdmin(record)}
                />
                )
              }
            </div>
          );
        },
      },
    ];
    return (
      <div className="container">
        <Card
          bordered={false}
          title={(
            <h3>
              <Icon type="team" />
              {' '}
               Admins
            </h3>
           )}
          extra={sending
            ? <Spin size="large" className="admins__spinner" />
            : (
              <Form
                layout="inline"
                className="admins__form"
                style={{ marginBottom: '1rem' }}
                onSubmit={this.inviteAdmin}
              >
                <Form.Item required>
                  <Input
                    style={{ width: 342 }}
                    prefix={<Icon type="mail" />}
                    title="Enter the admin email address, To send him invitation"
                    ref={this.inviteEmailRef}
                    type="email"
                    placeholder="admin@example.com"
                  />
                </Form.Item>
                <Form.Item>
                  <Button
                    type="dashed"
                    icon="user-add"
                    htmlType="submit"
                  >
                    Invite Admin
                  </Button>
                </Form.Item>
              </Form>
            )}
        >
          <Table
            pagination={pagination}
            loading={loading}
            dataSource={dataSource}
            columns={columns}
          />
        </Card>
      </div>);
  }
}
AdminsPage.defaultProps = {
  currentUser: null,
};

AdminsPage.propTypes = {
  loading: PropTypes.bool.isRequired,
  currentUser: PropTypes.object,
  users: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const usersHandle = Meteor.subscribe('AdminsTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!usersHandle.ready()) {
    return ({
      loading: true,
      users: [],
      pagination: {},
    });
  }
  const usersMeta = CollectionsMeta.findOne({ _id: 'Admins' });

  const users = Admins.find({}, {
    sort: { createdAt: -1 },
  });
  const totalUsers = usersMeta && usersMeta.count;
  // usersMeta.count;
  const usersExist = totalUsers > 0;
  return {
    loading: false,
    usersReady: usersHandle.ready(),
    users: usersExist ? users.fetch() : [],
    total: totalUsers,
    currentUser: Meteor.user(),
    pagination: {
      position: 'bottom',
      total: totalUsers,
      current: PageNumber.get(),
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(AdminsPage);
