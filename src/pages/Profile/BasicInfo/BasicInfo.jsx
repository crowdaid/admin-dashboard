import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import {
  message, Button, Input, Avatar, Form, Icon,
} from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import { Redirect } from 'react-router-dom';
import Files from '../../../api/Files';
import { validateMobileNum } from '../../../components/utils';

class BasicIngo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: '', lastName: '', mobile: '', changed: false, avatar: '', redirect: false,
    };

    // set the title of the page
    document.title = 'Profile | Crowdaid';
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFileChange = this.handleFileChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillReceiveProps(props) {
    // eslint-disable-next-line react/prop-types
    const { user, avatar } = props;
    if (user) {
      this.setState({
        firstName: user.profile.firstName || '',
        lastName: user.profile.lastName || '',
        mobile: user.profile.mobile || '',
        avatar,
      });
    } else {
      this.setState({ redirect: true });
    }
  }

  handleInputChange(e) {
    const { name } = e.currentTarget;
    const { value } = e.currentTarget;
    this.setState({ [name]: value, changed: true });
  }

  handleFileChange(e) {
    const file = e.currentTarget.files[0];
    this.uploadAvatar(file);
  }

  handleSubmit(e) {
    e.preventDefault();
    const { userId } = this.props;
    const { mobile } = this.state;
    if (mobile && !validateMobileNum(mobile)) {
      message.warning('Please add a valid mobile number');
      return;
    }
    Meteor.call('users.methods.update_profile', { userId, ...this.state }, (err, res) => {
      if (err) message.error(`Some this went wrong, ${err.reason}`);
      else {
        message.success('Your profile updated successfully', 10);
        this.setState({ changed: false });
      }
    });
  }

  uploadAvatar(file) {
    const { user } = this.props;
    Files.insert({
      file,
      meta: {
        namespace: 'users',
        slug: user._id,
        type: 'avatar',
      },
      onUploaded: (err, f) => {
        if (!err) {
          Meteor.call('users.methods.update_avatar', { userId: user._id, fileId: f._id });
        }
      },
      onError: (err, f) => {
        message.error(`Error on uploading ${f.name}, ${err.reason}`);
      },
    });
  }

  render() {
    const {
      firstName, lastName, mobile, changed, avatar, redirect,
    } = this.state;
    if (redirect) return <Redirect to="/404" />;
    return (
      <div className="profile">
        <h3 className="profile__title">Public Profile</h3>
        <form className="flex column" onSubmit={this.handleSubmit}>
          <div className="flex row">
            <div className="flex column">
              <div className="form-group">
                <Form.Item label="First Name">
                  <Input value={firstName} name="firstName" onChange={this.handleInputChange} />
                </Form.Item>
              </div>
              <div className="form-group">
                <Form.Item label="Last Name">
                  <Input value={lastName} name="lastName" onChange={this.handleInputChange} />
                </Form.Item>
              </div>
            </div>
            <div className="profile__avatar">
              {avatar
                ? <Avatar src={avatar} alt="avatar" size={135} />
                : (
                  <Avatar
                    style={{ backgroundColor: '#6c5ce7', fontSize: '6rem', verticalAlign: 'middle' }}
                    alt="avatar"
                    size={120}
                  >
                    {firstName[0] && firstName.trim()[0].toUpperCase()}
                  </Avatar>
                )}
              <div className="profile__upload-btn-wrapper">
                <Button>
                  <Icon type="picture" />
                  Change Avatar
                </Button>
                <input type="file" name="files" size="20" onChange={this.handleFileChange} />
              </div>
            </div>
          </div>
          <div style={{ width: 250 }} className="form-group">
            <Form.Item label="Mobile Number">
              <Input value={mobile} name="mobile" onChange={this.handleInputChange} />
            </Form.Item>
          </div>
          {changed
            ? <Button className="profile__btn" htmlType="submit" type="primary">Save</Button>
            : <Button className="profile__btn" disabled htmlType="submit" type="primary">Save</Button>}
        </form>
      </div>
    );
  }
}

export default withTracker((props) => {
  // get the user id for the profile link if exists
  const { userId } = props;
  const userHandle = Meteor.subscribe('loggedUser', userId);
  const avatarHandle = Meteor.subscribe('files.avatar', userId);
  if (!userHandle.ready() && !avatarHandle.ready()) {
    return {
      user: '',
    };
  }
  // get user profile
  const user = Meteor.users.findOne({ _id: userId || Meteor.userId() }, { fields: { profile: 1 } });
  if (!(user && user.profile)) {
    return {
      user: '',
    };
  }
  // load the user avatar
  const avatar = Files.findOne({ _id: user.profile.avatar, 'meta.type': 'avatar' });
  return {
    ready: true,
    user: user || '',
    avatar: (avatar && avatar.link()) || '',
  };
})(BasicIngo);
