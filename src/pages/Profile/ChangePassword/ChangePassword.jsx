/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';
import {
  message, Input, Button, Form,
} from 'antd';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = { changed: false };
    // set the title of the page
    document.title = 'Change Password | Crowdaid';
    // bind functions to class
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(e) {
    this.setState({ changed: true });
  }

  handleSubmit(e) {
    e.preventDefault();
    const oldPassword = e.target.oldPassword.value;
    const newPassword = e.target.newPassword.value;
    const newPassword2 = e.target.newPassword2.value;
    const { userId } = this.props;
    if (!oldPassword) {
      message.warning('Please enter your old password');
      return;
    }
    if (!newPassword || newPassword.length < 6) {
      message.warning('Please enter your new password at least 6 characters');
      return;
    }
    if (newPassword !== newPassword2) {
      message.error('Passwords does not match');
      return;
    }
    e.target.reset();
    if (!userId) {
      Accounts.changePassword(oldPassword, newPassword, (err) => {
        if (err) message.error(err.reason);
        else message.success('Your Password changed successfully');
      });
    } else {
      Meteor.call('users.methods.change_password', { userId, oldPassword, newPassword }, (err, res) => {
        if (err) message.error(`${err.reason}`);
        else {
          message.success('User Password changed successfully', 10);
          this.setState({ changed: false });
        }
      });
    }
    this.setState({ changed: false });
  }

  render() {
    const { changed } = this.state;
    const { userId } = this.props;
    return (
      <div className="profile">
        <h3 className="profile__title">
          Change password
          {userId && <p className="profile__note">You can change this user password by enter your current password as an old password</p>}
        </h3>
        <form className="flex column" onSubmit={this.handleSubmit}>
          <div className="form-group">
            <Form.Item required style={{ width: 355 }} label="Old Password">
              <Input type="password" name="oldPassword" onChange={this.handleInputChange} />
            </Form.Item>
          </div>
          <div className="flex row">
            <div className="form-group">
              <Form.Item required label="New Password">
                <Input type="password" name="newPassword" onChange={this.handleInputChange} />
              </Form.Item>
            </div>
            <div className="form-group">
              <Form.Item required label="Confirm New Password">
                <Input type="password" name="newPassword2" onChange={this.handleInputChange} />
              </Form.Item>
            </div>
          </div>
          {changed
            ? <Button className="profile__btn" htmlType="submit" type="primary">Update password</Button>
            : <Button className="profile__btn" disabled htmlType="submit" type="primary">Update password</Button>}
        </form>
      </div>
    );
  }
}

export default ChangePassword;
