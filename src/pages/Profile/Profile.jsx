import React, { Component } from 'react';
import ProtoTypes from 'prop-types';
import { Tabs } from 'antd';
import BasicInfo from './BasicInfo';
import Emails from './Emails';
import ChangePassword from './ChangePassword';

// TODO: add donor interests in his profile
// TODO: update interests on profile update
// TODO: add isDonor verification
// TODO: add IDNumber if user is not Donor

class Profile extends Component {
  constructor(props) {
    super(props);
    // set the title of the page
    document.title = 'Profile | Crowdaid';
  }

  render() {
    const { match: { params: { id } } } = this.props;
    return (
      <div className="container">
        <Tabs
          type="card"
          animated={false}
          style={{ width: '70%' }}
          tabPosition="left"
          defaultActiveKey="1"
          size="default"
        >
          <Tabs.TabPane key="1" id="basicInfo" tab="Profile">
            <BasicInfo userId={id} />
          </Tabs.TabPane>
          <Tabs.TabPane key="2" id="Emails" tab="Emails">
            <Emails userId={id} />
          </Tabs.TabPane>
          <Tabs.TabPane key="3" id="ChangePassword" tab="Change Password">
            <ChangePassword userId={id} />
          </Tabs.TabPane>
        </Tabs>
      </div>
    );
  }
}

Profile.propTypes = {
  match: ProtoTypes.instanceOf(Object).isRequired,
};
export default Profile;
