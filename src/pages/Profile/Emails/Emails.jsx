import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import {
  message, Modal, List, Button, Form, Input, Icon, Spin,
} from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import { validateEmail } from '../../../components/utils';


class Emails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userId: '', ready: false, emails: [], resending: false,
    };
    // set the title of the page
    document.title = 'Emails | Crowdaid';
    this.handleAddEmail = this.handleAddEmail.bind(this);
    this.handleRemoveEmail = this.handleRemoveEmail.bind(this);
    this.handleResendEmail = this.handleResendEmail.bind(this);
  }

  componentWillReceiveProps(props) {
    // eslint-disable-next-line react/prop-types
    const { emails, ready, userId } = props;
    this.setState({
      emails,
      ready,
      userId,
    });
  }

  handleRemoveEmail = (email) => {
    const { userId } = this.state;
    Modal.confirm({
      title: `Are you sure you want to remove this email ${email}`,
      onOk() {
        Meteor.call('users.methods.remove_email', { userId, email }, (err) => {
          if (err) message.error(err.reason);
          else message.success('Email removed successfully');
        });
      },
      onCancel() {},
    });
  };

  handleResendEmail = (email) => {
    const { userId } = this.state;
    this.setState({ resending: true });
    Meteor.call('users.methods.send_verification', { userId, email }, (err) => {
      if (err) message.error(err.reason);
      else {
        this.setState({ resending: false });
        message.success('verification email resent successfully');
      }
    });
  };

  handleAddEmail(e) {
    e.preventDefault();
    const { emails, userId } = this.state;
    const email = e.target.email.value;
    if (!validateEmail) {
      message.error('Please enter a valid email');
      return;
    }
    // check if he have this email already
    if (emails.filter(em => em.address === email).length !== 0) {
      message.warning('Already have this email');
      return;
    }
    e.target.reset();
    Meteor.call('users.methods.add_email', { userId, email }, (err) => {
      if (err) message.error(err.reason);
      else {
        message.success('Email added successfully, Please verify the new email', 10);
      }
    });
  }

  render() {
    const { ready, emails, resending } = this.state;
    return (
      <div className="profile">
        <h3 className="profile__title">
          Emails
          {resending && (
          <span>
            {' '}
            <Spin />
          </span>
          )}
        </h3>
        {ready ? (
          <List
            size="small"
            bordered
            dataSource={emails}
            renderItem={email => (
              <List.Item className="emails__card">
                <table className="profile__table__emails">
                  <tbody>
                    <tr>
                      <td>{email.address}</td>
                      {email.verified
                        ? <td className="emails__true">Verified</td>
                        : (
                          <td className="emails__false">
                            Not verified
                            <div title="Resend verification email">
                              {!resending
                              && (
                              <Icon
                                type="reload"
                                className="emails__resend"
                                onClick={e => this.handleResendEmail(email.address)}
                              />
                              )
                            }
                            </div>
                          </td>
                        )
                        }
                      <td style={{ width: '7%' }} title="Remove Email">
                        <Icon
                          type="delete"
                          theme="filled"
                          className="emails__icon"
                          onClick={e => this.handleRemoveEmail(email.address)}
                        />
                      </td>
                    </tr>
                  </tbody>
                </table>

              </List.Item>
            )}
          />
        ) : <Spin size="large" className="form__spinner" />}
        <form className="emails__add" onSubmit={this.handleAddEmail}>
          <Form.Item label="Add new email" required>
            <Input
              type="email"
              name="email"
              prefix={<Icon type="mail" />}
              placeholder="email@example.com"
              addonAfter={(
                <Button
                  type="ghost"
                  style={{ border: 'none' }}
                  htmlType="submit"
                  title="Add"
                >
                  Add
                </Button>
                )}
            />
          </Form.Item>
        </form>
      </div>
    );
  }
}

Emails.defaultProps = {
  Emails: [],
};

export default withTracker((props) => {
  // get the user id for the profile link if exists
  const { userId } = props;
  const userHandle = Meteor.subscribe('loggedUser', userId);
  if (!userHandle.ready()) {
    return {
      ready: false,
      emails: [],
    };
  }

  // get user emails
  const user = Meteor.users.findOne({ _id: userId || Meteor.userId() }, { fields: { emails: 1 } });
  if (!user) {
    return {
      user: '',
    };
  }
  return {
    ready: true,
    emails: (user && user.emails) || [],
  };
})(Emails);
