import React, { Component } from 'react';
import { Accounts } from 'meteor/accounts-base';
import { Spin, Icon } from 'antd';

class VerifyEmail extends Component {
  constructor(props) {
    super(props);
    this.state = { done: false, err: '' };
    // set the title of the page
    document.title = 'Emails | Crowdaid';
  }

  componentDidMount(e) {
    // eslint-disable-next-line react/prop-types
    const { match: { params: { token } }, history } = this.props;
    Accounts.verifyEmail(token, ((err) => {
      if (err) {
        history.replace('/');
      } else {
        this.setState({ done: true });
        setTimeout(() => {
          history.replace('/');
        }, 4000);
      }
    }));
  }

  render() {
    const { done, err } = this.state;
    if (err) {
      return (
        <div className="verifyemail__verified--error">
          Something went wrong please try again
        </div>
      );
    }
    return (
      <div className="verifyemail">
        {done ? (
          <div className="verifyemail__verified">
            <Icon type="check" className="verifyemail__verified--icon" />
            Email verified successfully
          </div>

        ) : (
          <div className="verifyemail__verifying">
            <Spin size="large" />
            verifying email...
          </div>
        )}
      </div>
    );
  }
}

export default VerifyEmail;
