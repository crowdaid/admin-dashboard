import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import CategoriesCollection from '../../../api/Categories';
import Form from '../Form';

const Edit = ({ loading, ...props }) => {
  if (loading) return (<Spin size="large" />);
  return (<Form {...props} formType="edit" />);
};

Edit.defaultProps = {
  parentCategory: null,
  aliases: [],
  categories: [],
  icon: '',
};

Edit.propTypes = {
  categories: PropTypes.array,
  loading: PropTypes.bool.isRequired,
  parentCategory: PropTypes.string,
  aliases: PropTypes.array,
  name: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  icon: PropTypes.string,
};

export default withTracker((props) => {
  const categoriesHandle = Meteor.subscribe('categories.all');
  const { match: { params: { id } } } = props;

  if (!categoriesHandle.ready()) {
    return {
      categories: [],
      loading: true,
      parentCategory: '',
      aliases: [],
      name: '',
      description: '',
      icon: '',
    };
  }

  const categories = CategoriesCollection.find({ isRemoved: false, _id: { $ne: id } }).fetch();

  const category = CategoriesCollection.findOne({ _id: id });

  return {
    categories,
    id,
    loading: false,
    parentCategory: category.parentCategory,
    aliases: category.aliases,
    name: category.name,
    categoryType: category.categoryType,
    description: category.description,
    icon: category.icon,
  };
})(Edit);
