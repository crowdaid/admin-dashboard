import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Tag, Button,
} from 'antd';
import { CollectionsMeta } from '../../../api/CollectionsMeta';
import CategoriesCollection from '../../../api/Categories';
import { searchTables } from '../../../components/tablesFilters';
import MergCats from '../../../components/MergCats';
// eslint-disable-next-line import/no-self-import
import Self from './List';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

const removeCategory = (category) => {
  Modal.confirm({
    iconType: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove category ${category.name}?`,
    content: 'This category and its children will be cleared from all cases and marked as removed.',
    onOk() {
      Meteor.call('categories.methods.remove', { _id: category.id }, (err, res) => {
        if (err) {
          message.error(err.reason, 7);
        } else {
          message.success('Category removed successfully');
        }
      });
    },
    onCancel() {},
  });
};

class CategoriesList extends Component {
  constructor(props) {
    super(props);
    this.state = { searchText: '', mergeModal: { visible: false, parent: null } };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
    // set the title of the page
    document.title = 'Categories | Crowdaid';
  }

  render() {
    const {
      categories, pagination, loading, history,
    } = this.props;

    const dataSource = categories.map(category => ({
      id: category._id,
      parentCategory: category.parentCategory,
      name: category.name,
      categoryType: category.categoryType,
      description: category.description || '-',
      aliases: (category.aliases && category.aliases.length) ? category.aliases.reduce((a, b) => `${a}, ${b}`) : '-',
      key: category._id,
      icon: category.icon,
    }));

    const columns = [
      {
        title: 'Name',
        dataIndex: 'name',
        key: 'name',
        ...this.getColumnSearchProps('name', 'Names', this),
      },
      {
        title: 'Descriptions',
        dataIndex: 'description',
        key: 'description',
      },
      {
        title: 'Aliases',
        dataIndex: 'aliases',
        key: 'aliases',
        ...this.getColumnSearchProps('aliases', 'aliases', this),
      },
      {
        title: 'Icon',
        key: 'Icon',
        render: record => (record.icon
          ? <i style={{ fontSize: 18 }} className={record.icon.class} /> : <Tag>No Icon</Tag>),
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => {
          if (record.isRemoved) {
            return (<Tag color="red"> removed </Tag>);
          }
          return (
            <span>
              <Button
                className="categories__button"
                type="primary"
                title="Edit"
                shape="circle-outline"
                icon="edit"
                onClick={() => history.push(`/categories/${record.id}/edit`)}
              />
              <Button
                className="categories__button"
                type="danger"
                title="Remove"
                shape="circle-outline"
                icon="delete"
                onClick={() => removeCategory(record)}
              />
              <Button
                className="categories__button"
                title="Merge"
                icon="fork"
                onClick={() => this.setState({ mergeModal: { visible: true, parent: record } })}
              >
                  Merge
              </Button>
            </span>
          );
        },
      },
    ];
    const { showJustTable } = this.props;
    const { mergeModal } = this.state;

    if (showJustTable) {
      if (!categories.length) return <p>No sup categories for this one</p>;
      return (
        <div>

          <Table
            size="small"
            loading={loading}
            pagination={false}
            dataSource={dataSource}
            columns={columns}
          />
          <MergCats
            visible={mergeModal.visible}
            parent={mergeModal.parent}
            handleCancel={() => this.setState({ mergeModal: { visible: false, parent: null } })}
          />
        </div>

      );
    }
    return (
      <div>
        <Table
          loading={loading}
          pagination={pagination}
          dataSource={dataSource}
          columns={columns}
          expandedRowRender={record => (
            <Self
              showJustTable
              categoryType={record.categoryType}
              parentCategory={record.id}
            />
          )}
        />
        <MergCats
          visible={mergeModal.visible}
          parent={mergeModal.parent}
          handleCancel={() => this.setState({ mergeModal: { visible: false, parent: null } })}
        />
      </div>
    );
  }
}

CategoriesList.propTypes = {
  loading: PropTypes.bool.isRequired,
  categories: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker(({ parentCategory, categoryType }) => {
  const categoriesHandle = Meteor.subscribe('CategoriesTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!categoriesHandle.ready()) {
    return ({
      loading: true,
      categories: [],
      pagination: {},
    });
  }
  const categoriesMeta = CollectionsMeta.findOne({ _id: 'Categories' });

  const filters = { parentCategory: parentCategory || null, categoryType };
  const categories = CategoriesCollection.find(filters, {
    sort: { createdAt: -1 },
  });

  const totalCategories = categoriesMeta && categoriesMeta.count;

  const categoriesExist = totalCategories > 0;

  return {
    loading: false,
    usersReady: categoriesHandle.ready(),
    categories: categoriesExist ? categories.fetch() : [],
    total: totalCategories,
    pagination: {
      position: 'bottom',
      total: totalCategories,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(withRouter(CategoriesList));
