import React from 'react';
import {
  Tabs, Card, Icon, Button,
} from 'antd';
import './List/List.css';
import CategoriesList from './List/List';

const getTab = ({ search }) => {
  const tabKey = search.substring(1);
  if (['causes', 'needTypes', 'items'].includes(tabKey)) {
    return tabKey;
  }
  return 'causes';
};
const changeTab = (key, history) => {
  history.replace(`/categories?${key}`);
};


const CategoriesTypesTabs = ({ history, location }) => (
  <Card
    bordered={false}
    title={(
      <h3>
        <Icon type="tags" />
        {' '}
        Categories
      </h3>
        )}
    extra={<Button type="primary" onClick={() => history.push('/categories/add')} icon="plus"> Add New</Button>}
  >
    <Tabs
      onTabClick={key => changeTab(key, history)}
      defaultActiveKey="causes"
      activeKey={getTab(location)}
    >
      <Tabs.TabPane
        tab={(
          <span>
            <Icon type="flag" />
            Casues
          </span>)}
        key="causes"
      >
        <CategoriesList categoryType="cause" />
      </Tabs.TabPane>
      <Tabs.TabPane
        tab={(
          <span>
            <Icon type="bars" />
            Need Types
          </span>)}
        key="needTypes"
      >
        <CategoriesList categoryType="needType" />
      </Tabs.TabPane>
      <Tabs.TabPane
        tab={(
          <span>
            <Icon type="shopping-cart" />
            Items
          </span>)}
        key="items"
      >
        <CategoriesList categoryType="item" />
      </Tabs.TabPane>
    </Tabs>
  </Card>
);

export default CategoriesTypesTabs;
