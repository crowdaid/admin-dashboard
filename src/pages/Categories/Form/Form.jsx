import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import {
  Col, Form as AntForm, Row, Select, Button, Input, message, Card,
} from 'antd';
import EditableTagGroup from '../../../components/EditableTagGroup/EditableTagGroup';
import IconPicker from '../../../components/IconPicker';

const categoryTypes = [
  { value: 'cause', name: 'Cause' },
  { value: 'needType', name: 'Need Type' },
  { value: 'item', name: 'Item' }];

class Form extends React.Component {
  constructor(props) {
    super(props);

    const {
      name, description, parentCategory, aliases, icon, categoryType,
    } = this.props;

    this.state = {
      name,
      description,
      matchNames: [],
      parentCategory,
      aliases,
      categoryType,
      icon,
      iconPicker: false,
    };
    // binding handlers
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
    this.handleCategoryType = this.handleCategoryType.bind(this);
    this.handleNameSearch = this.handleNameSearch.bind(this);
    this.handleAliasChange = this.handleAliasChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleIconPickerShow = this.handleIconPickerShow.bind(this);
    this.handleIconSelected = this.handleIconSelected.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  /**
     * handling changed parentCategory
     * @param parentCategory
     */
  handleCategoryChange(parentCategory) {
    const { categories, id } = this.props;
    if (id) {
      // check if the parent of the current parent is the this category
      const crossParents = categories.filter(p => (p._id === parentCategory)
      && (p.parentCategory === id));
      if (crossParents.length) {
        message.error('The parent you selected his parent is this category');
        return;
      }
    }
    this.setState({
      parentCategory,
    });
  }

  handleCategoryType(categoryType) {
    this.setState({
      categoryType,
      parentCategory: null,
    });
  }

  handleIconSelected(icon) {
    this.setState({
      icon: {
        class: `icon-${icon.css}`,
        id: icon.uid,
      },
      iconPicker: false,
    });
  }

  /**
     * handling aliases changing
     * @param alias
     */
  handleAliasChange(alias) {
    this.setState({
      aliases: alias,
      iconPicker: false,
    });
  }

  handleNameSearch(e) {
    const keyword = e.target.value;
    const { match: { params: { id } } } = this.props;
    this.setState({ name: keyword });
    if (keyword.length >= 4) {
      setTimeout(() => {
        Meteor.call('categories.methods.search', { keyword: keyword.trim(), id }, (err, matchNames) => {
          this.setState({ matchNames: matchNames || [] });
        });
      }, 500);
    } else {
      this.setState({ matchNames: [] });
    }
  }

  /**
     * Handling normal fields changes
     * @param event
     */
  handleChange(event) {
    if (event.target.name === 'name' && !event.target.value) {
      this.setState({
        [event.target.name]: event.target.value,
        error: 'Name is required',
      });
    } else {
      this.setState({
        [event.target.name]: event.target.value,
        error: '',
      });
    }
  }

  handleIconPickerShow(e) {
    e.preventDefault();
    this.setState({ iconPicker: true });
  }

  /**
     * Update category information
     * @param e
     */
  handleSubmit(e) {
    e.preventDefault();
    const { history, formType, match: { params: { id } } } = this.props;
    const {
      parentCategory, description, aliases, name, icon, categoryType,
    } = this.state;
    if (!name) {
      message.error('Name is required');
      return;
    }
    if (!categoryType) {
      message.error('Category type is required');
      return;
    }

    if (formType === 'create') {
      Meteor.call('categories.methods.create', {
        parentCategory: parentCategory || null,
        description,
        aliases,
        categoryType,
        name: name.trim(),
        icon,
      }, (error, result) => {
        if (error) {
          message.error(error.reason);
        } else {
          this.setState({ error: '' });
          history.push(`/categories?${categoryType}s`);
          const as = parentCategory ? 'as sub category' : 'as parent category';
          message.success(`Category add ${as} successfully`);
        }
      });
    } else {
      Meteor.call('categories.methods.update', {
        _id: id,
        parentCategory: parentCategory || null,
        description,
        aliases,
        categoryType,
        name: name.trim(),
        icon,
      }, (error, result) => {
        if (error) {
          message.error(error.reason);
        } else {
          this.setState({ error: '' });
          history.push(`/categories?${categoryType}s`);
          message.success('Category updated successfully');
        }
      });
    }
  }

  renderMatches() {
    const { matchNames } = this.state;
    if (matchNames.length !== 0) {
      return (
        <div>
          <h2 className="categories__matches-header">These categories may partially match the entered name</h2>
          <ul className="matches-ul">
            {matchNames.map(match => (
              <li key={match._id}>
                <b>{match.name}</b>
                {` in ${match.categoryType}s`}
              </li>
            ))}
          </ul>
        </div>
      );
    }
    return null;
  }

  render() {
    const {
      categories, id, formType, history,
    } = this.props;
    const {
      name, description, parentCategory, aliases,
      error, iconPicker, icon, categoryType,
    } = this.state;
    const filteredCategories = categories.filter(c => (c._id !== id)
    && (c.categoryType === categoryType));
    console.log(icon);
    return (
      <div className="container">
        { error && <b>{ error }</b>}
        <Card
          title={formType === 'edit' ? `Edit category: (${name})` : 'Create new category'}
          extra={<Button icon="arrow-left" title="Back" onClick={() => history.goBack()}>Back</Button>}
        >
          <Row>
            <Col span={12}>
              {this.renderMatches()}
              <div className="form-group">
                <AntForm.Item label="Name" required>
                  <Input className="fill" name="name" onChange={this.handleNameSearch} value={name} />
                </AntForm.Item>
              </div>
            </Col>
            <Col span={12}>
              <div className="form-group">
                <AntForm.Item label="Category type" required>
                  <Select
                    style={{ width: 250 }}
                    placeholder="Select category type"
                    className="fill"
                    value={categoryType}
                    onChange={this.handleCategoryType}
                  >
                    {categoryTypes.map(type => (
                      <Select.Option value={type.value} key={type.value}>
                        {type.name}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
            </Col>
            {categoryType && categoryType !== 'item' && (
            <Col span={12}>
              <div className="form-group">
                <AntForm.Item label="Parent category">
                  <Select
                    showSearch
                    style={{ width: 200 }}
                    placeholder="Select Parent category"
                    className="fill"
                    value={parentCategory}
                    optionFilterProp="children"
                    onChange={this.handleCategoryChange}
                    filterOption={(input, option) => option.props.children
                      .toLowerCase().indexOf(input.toLowerCase()) >= 0}
                  >
                    {filteredCategories.map(c => (
                      <Select.Option value={c._id} key={c._id}>
                        {c.name}
                      </Select.Option>
                    ))}
                  </Select>
                </AntForm.Item>
              </div>
            </Col>
            )}
          </Row>
          <Row>
            <Col span={24}>
              <div className="form-group">
                <AntForm.Item label="Description">
                  <Input.TextArea
                    name="description"
                    className="fill"
                    onChange={this.handleChange}
                    value={description}
                  />
                </AntForm.Item>
              </div>
            </Col>
          </Row>
          <Row>
            <Col span={8}>
              <div className="form-group">
                <AntForm.Item label="Aliases">
                  <EditableTagGroup
                    tags={aliases}
                    placeholder="Add Alias"
                    handleTagsChanged={this.handleAliasChange}
                  />
                </AntForm.Item>
              </div>
            </Col>
            <Col span={8}>
              <div className="form-group">
                <AntForm.Item label="Pick a category icon">
                  <Button onClick={this.handleIconPickerShow} type="dashed">
                    {icon ? <i style={{ fontSize: 21 }} className={icon.class} /> : 'Choose' }
                  </Button>
                </AntForm.Item>
              </div>
            </Col>
          </Row>
          <Button block htmlType="button" type="primary" onClick={this.handleSubmit}>

                Save
          </Button>
        </Card>
        <IconPicker handlePick={this.handleIconSelected} visible={iconPicker} />
      </div>
    );
  }
}

export default withRouter(Form);
