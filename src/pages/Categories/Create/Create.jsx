import React from 'react';
import { Spin } from 'antd';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import CategoriesCollection from '../../../api/Categories';
import Form from '../Form';

const Create = ({ loading, ...props }) => {
  if (loading) return (<Spin size="large" />);
  return (<Form {...props} formType="create" />);
};

Create.defaultProps = {
  parentCategory: null,
  aliases: [],
  name: '',
  description: '',
  categories: [],
};

Create.propTypes = {
  categories: PropTypes.array,
  loading: PropTypes.bool.isRequired,
  parentCategory: PropTypes.string,
  aliases: PropTypes.array,
  name: PropTypes.string,
  description: PropTypes.string,
};

export default withTracker((props) => {
  const categoriesHandle = Meteor.subscribe('categories.all');
  if (!categoriesHandle.ready()) {
    return {
      categories: [],
      loading: true,
      aliases: [],
      name: '',
      description: '',
    };
  }

  const categories = CategoriesCollection.find({ parentCategory: null }).fetch();

  return {
    categories,
    loading: false,
    aliases: [],
    name: '',
    description: '',
  };
})(Create);
