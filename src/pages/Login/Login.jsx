/* eslint-disable import/no-extraneous-dependencies */
import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Meteor } from 'meteor/meteor';
import { validateEmail, loginAs } from '../../components/utils';

// TODO check the user if he an admin befor login (solved in server)

class Login extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.state = { error: '' };
    // eslint-disable-next-line react/prop-types
    const { history } = this.props;
    // set the title of the page
    document.title = 'Login | Crowdaid';
    // redirect if the user already logged in
    if (Meteor.userId()) {
      history.replace('/');
    }
  }

  // handle login request
  handleSubmit(e) {
    e.preventDefault();
    const email = e.target.user.value.trim();
    const password = e.target.password.value;

    // validate email
    if (!email || !validateEmail(email)) {
      this.setState({ error: 'The Email must be vaild, please check it!' });
      return;
    }
    // eslint-disable-next-line react/prop-types
    const { history } = this.props;
    // login the user if he is an admin
    loginAs(email, password, ['admin', 'superadmin'], (error) => {
      if (!error) {
        history.replace('/');
      } else {
        this.setState({ error: error.reason });
      }
    });
  }

  render() {
    const { error } = this.state;
    return (
      <div className="register">
        <div className="register__wrapper">
          <div className="register__title">Login</div>
          {error && <p className="register__error">{error}</p>}
          <form className="register__form" onSubmit={this.handleSubmit}>
            <input required type="email" name="user" placeholder="Enter your email" />
            <input required type="password" name="password" placeholder="Enter your password" />
            <button type="submit" className="register__btn">Login</button>
            <p className="register__info">If you do not have an account please contact the administrator to add you</p>
          </form>
        </div>
      </div>
    );
  }
}

export default withRouter(Login);
