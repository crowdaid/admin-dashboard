import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, message, Button, Tag, Card, Icon,
} from 'antd';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import PromptModal from './PromptModal';
import { validateEmail } from '../../components/utils';
import Owners from '../../api/Owners';
import { searchTables } from '../../components/tablesFilters';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);


class OwnersPage extends Component {
  constructor(props) {
    super(props);

    this.state = { searchText: '', promptVisable: false, owner: '' };
    this.closePrompt = this.closePrompt.bind(this);
    this.removeOwner = this.removeOwner.bind(this);
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
    // set the title of the page
    document.title = 'Owners | Crowdaid';
  }

  removeOwner = (newOwner) => {
    const { owner } = this.state;

    if (!validateEmail(newOwner)) {
      message.error('The new owner must be a valid email');
      return;
    }
    Meteor.call('organizations.methods.removeOwner', {
      userId: owner._id, newOwner,
    }, (err, res) => {
      if (err) {
        message.error('something went wrong');
      } else {
        this.setState({ promptVisable: false });
        message.success('The new owner set successfully');
      }
    });
  };

  openPrompt(owner) {
    this.setState({ promptVisable: true, owner });
  }

  closePrompt() {
    this.setState({ promptVisable: false });
  }

  render() {
    const {
      owners, pagination, loading, history,
    } = this.props;

    const dataSource = owners.map(owner => ({
      ...owner,
      firstName: (owner.profile && owner.profile.firstName) || '-',
      lastName: (owner.profile && owner.profile.lastName) || '-',
      mobile: (owner.profile && owner.profile.mobile) || '-',
      ID: (owner.profile && owner.profile.idNumber) || '-',
      key: owner._id,
      email: owner.emails[0].address,
    }));

    const columns = [
      {
        title: 'First name',
        dataIndex: 'firstName',
        key: 'firstName',
        ...this.getColumnSearchProps('firstName', 'first name', this),
      },
      {
        title: 'Last name',
        dataIndex: 'lastName',
        key: 'lastName',
        ...this.getColumnSearchProps('lastName', 'last name', this),
      },
      {
        title: 'Mobile',
        dataIndex: 'mobile',
        key: 'mobile',
        ...this.getColumnSearchProps('mobile', 'mobiles', this),
      },
      {
        title: 'ID',
        dataIndex: 'ID',
        key: 'ID',
        ...this.getColumnSearchProps('ID', 'IDs', this),
      },
      {
        title: 'Email Address',
        dataIndex: 'email',
        key: 'email',
        ...this.getColumnSearchProps('email', 'emails', this),
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => (
          <div className="owners__actions">
            {record.profile
              ? (
                <Button
                  shape="circle-outline"
                  type="primary"
                  title="view & edit Profile"
                  icon="solution"
                  onClick={() => history.push(`/users/${record._id}`)}
                />
              )
              : <Tag color="cyan">No profile yet</Tag>
              }
            <Button
              type="danger"
              shape="circle-outline"
              title="Remove"
              icon="delete"
              onClick={() => this.openPrompt(record)}
            />
          </div>
        ),
      },
    ];
    const { promptVisable, owner } = this.state;
    return (
      <div className="container">
        <Card
          bordered={false}
          title={(
            <h3>
              <Icon type="crown" />
              {' '}
               Organization&#39;s Owners
            </h3>
          )}
        >
          <Table
            pagination={pagination}
            loading={loading}
            dataSource={dataSource}
            columns={columns}
          />
          <PromptModal
            title={owner && `Remove owner ${owner.emails[0].address}`}
            visible={promptVisable}
            onOk={this.removeOwner}
            owner={owner}
            onCancel={this.closePrompt}
          />
        </Card>
      </div>
    );
  }
}

OwnersPage.propTypes = {
  loading: PropTypes.bool.isRequired,
  owners: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const usersHandle = Meteor.subscribe('OwnersTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!usersHandle.ready()) {
    return ({
      loading: true,
      owners: [],
      pagination: {},
    });
  }
  const usersMeta = CollectionsMeta.findOne({ _id: 'Owners' });

  const owners = Owners.find({}, {
    sort: { createdAt: -1 },
  });

  const totalRecords = usersMeta && usersMeta.count;

  return {
    loading: false,
    owners: totalRecords ? owners.fetch() : [],
    total: totalRecords,
    pagination: {
      position: 'bottom',
      total: totalRecords,
      current: PageNumber.get(),
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(OwnersPage);
