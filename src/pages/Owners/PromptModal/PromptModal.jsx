import React from 'react';
import {
  Modal, message, Input, Icon,
} from 'antd';
import PropTypes from 'prop-types';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import Organizations from '../../../api/Organizations';
import { validateEmail } from '../../../components/utils';

class PromptModal extends React.Component {
  constructor(props) {
    super(props);
    this.removeInput = React.createRef();
  }

  handleOk = (e) => {
    const { onOk } = this.props;
    const inputText = this.removeInput.current.input.value;
    if (!validateEmail(inputText)) {
      message.error('The new owner must be a valid email');
      return;
    }
    this.removeInput.current.value = '';
    onOk(inputText);
  }

  render() {
    const {
      visible, onCancel, title, orgs,
    } = this.props;
    let ownerOrgs;
    if (orgs.length > 1) ownerOrgs = orgs.map(org => <b key={org._id}>{` ${org.name.ar || 'Nothing'}, `}</b>);
    else ownerOrgs = orgs.map(org => <b key={org._id}>{` ${org.name.ar || 'Nothing'}`}</b>);
    return (
      <div>
        <Modal
          title={title}
          visible={visible}
          onOk={this.handleOk}
          onCancel={onCancel}
        >
          <p className="prompt__note">
            This user is the owner of
            [
            {ownerOrgs}
            ]
            , by removing this user you will make the new entered user the new owner of
            [
            {[ownerOrgs]}
            ]
          </p>
          <p className="prompt__action">
            Enter the new owner email to send him an invitation
          </p>
          <Input
            ref={this.removeInput}
            type="email"
            prefix={<Icon type="mail" />}
            placeholder="email@example.com"
          />
          <Icon />
        </Modal>
      </div>
    );
  }
}

PromptModal.propTypes = {
  onOk: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  visible: PropTypes.bool.isRequired,
  orgs: PropTypes.array.isRequired,
};

export default withTracker((props) => {
  const { owner } = props;
  const orgsHandle = Meteor.subscribe('user.organizations', owner._id);
  if (!orgsHandle.ready()) {
    return {
      ready: true,
      orgs: [],
      activeOrg: '',
    };
  }
  const userOrgs = Organizations.find({}, { sort: { _id: 'DESC' } }).fetch();
  return {
    ready: true,
    orgs: userOrgs,
    activeOrg: owner.organizationId,
  };
})(PromptModal);
