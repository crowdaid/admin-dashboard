import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import {
  Spin, message, Card, Button, Form as AntForm, Input,
  DatePicker, Select, Icon, Avatar,
} from 'antd';
import Moment from 'moment';
import { validateEmail } from '../../../components/utils';
import FilesDisplayer from '../FilesDisplayer';
import Files from '../../../api/Files';
import GoogleMap from '../../../components/GoogleMap';

const dateFormat = 'DD/MM/YYYY';

class Form extends React.Component {
  constructor(props) {
    super(props);
    const { org } = props;
    this.state = {
      nameAr: org ? org.name.ar : '',
      nameEn: org ? org.name.en : '',
      owner: org ? org.owner : '',
      HQLocation: org ? org.HQLocation : { address: 'Cairo', location: { type: 'Point', coordinates: [30.0444, 31.235] } },
      about: org ? org.about : '',
      registrationCode: org ? org.registrationCode : '',
      registrationDate: org ? Moment(org.registrationDate, dateFormat) : Moment(),
      categories: org ? org.categories : [],
      legalFiles: [],
      filesCompleted: 0,
      filesFailed: 0,
      loading: false,
      logo: org && org.logo,
      previewLogo: '',
    };
    // set the title of the page
    document.title = 'Organizations | Crowdaid';
    // binding handlers
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCategoriesChange = this.handleCategoriesChange.bind(this);
    this.handleDateChange = this.handleDateChange.bind(this);
    this.handleFilesChange = this.handleFilesChange.bind(this);
    this.handleLogoChange = this.handleLogoChange.bind(this);
    this.uploadLogo = this.uploadLogo.bind(this);
    this.readURL = this.readURL.bind(this);
    this.handleMapChange = this.handleMapChange.bind(this);
  }

  handleChange(e) {
    this.setState({
      [e.target.name]: e.target.value,
    });
  }

  handleFilesChange(e) {
    const { files } = e.currentTarget;
    const formattedFiles = [];

    Object.keys(files).forEach((k, i) => {
      formattedFiles.push(files[k]);
    });

    this.setState({
      legalFiles: formattedFiles,
    });
  }

  changeFiles(org) {
    const { legalFiles } = this.state;
    if (legalFiles) {
      legalFiles.forEach((file) => {
        Files.insert({
          file,
          meta: {
            namespace: 'organizations',
            orgId: org._id,
            slug: org.slug,
            type: 'legalFiles',
          },
          onUploaded: (err, f) => {
            if (!err) {
              Meteor.call('organizations.methods.addLegalFile', { id: org._id, fileId: f._id });
              const { filesCompleted } = this.state;
              this.setState({
                filesCompleted: filesCompleted + 1,
              });
            }
          },
          onError: (err, f) => {
            const { filesFailed } = this.state;
            message.error(`Error on uploading ${f.name}, ${err.reason}`);
            this.setState({
              filesFailed: filesFailed + 1,
            });
          },
        });
      });
    }
  }

  readURL(file) {
    const reader = new FileReader();
    reader.onload = (e) => {
      this.setState({ previewLogo: e.target.result });
    };
    reader.readAsDataURL(file);
  }

  handleLogoChange(e) {
    const file = e.currentTarget.files[0];
    this.readURL(file);
    this.setState({ logo: file });
  }

  uploadLogo(org) {
    const { logo } = this.state;
    if (typeof logo === 'string' || !logo) return;

    Files.insert({
      file: logo,
      meta: {
        namespace: 'organizations',
        orgId: org._id,
        slug: org.slug,
        type: 'logo',
      },
      onUploaded: (err, f) => {
        if (!err) {
          Meteor.call('organizations.methods.update_logo', { orgId: org._id, fileId: f._id });
        }
      },
      onError: (err, f) => {
        message.error(`Error on uploading ${f.name}, ${err.reason}`);
      },
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    const { history, org, formType } = this.props;
    if (!this.isFormValid()) return;
    const {
      categories, nameAr, nameEn, legalFiles, registrationDate, ...neededFeilds
    } = this.state;

    const name = {
      ar: nameAr,
      en: nameEn,
    };
    this.setState({ loading: true });
    if (formType === 'create') {
      Meteor.call('organizations.methods.create', {
        categories, name, registrationDate: registrationDate.format('YYYY-MMM-DD'), ...neededFeilds,
      }, (err, res) => {
        this.setState({ loading: false });
        if (err) {
          message.error(err.reason);
          return;
        }
        this.changeFiles(res);
        this.uploadLogo(res);
        message.success('Organization added successfully');
        history.replace('/organizations');
      });
    } else {
      Meteor.call('organizations.methods.update', {
        _id: org._id, categories, registrationDate: registrationDate.format('YYYY-MMM-DD'), name, ...neededFeilds,
      }, (err, res) => {
        this.setState({ loading: false });
        if (err) {
          message.error(err.reason);
          return;
        }
        this.changeFiles(res);
        this.uploadLogo(res);
        message.success('Organization updated successfully');

        history.replace('/organizations');
      });
    }
  }

  isFormValid() {
    const {
      registrationDate, registrationCode, nameAr, nameEn, owner, about, HQLocation, logo,
    } = this.state;

    if (!nameAr.trim() || !nameEn.trim()) {
      message.error('English and Arabic names are required.');
      return false;
    }

    if (!registrationCode) {
      message.error('Registration Code is required.');
      return false;
    }

    if (!registrationDate) {
      message.error('Registration Date is required.');
      return false;
    }
    if (about && about.trim().length < 10) {
      message.error('The about section should be at least 10 characters, if exists');
      return false;
    }
    if (!HQLocation) {
      message.error('Please set the HQ location');
      return false;
    }
    if (HQLocation && HQLocation.location.coordinates && !HQLocation.address) {
      message.error("The HQ location should't have a location without address");
      return false;
    }
    if (!logo) {
      message.error('The organization must have a logo');
      return false;
    }
    if (!validateEmail(owner)) {
      message.error('The owner must be a valid email');
      return false;
    }

    return true;
  }

  handleCategoriesChange(categories) {
    this.setState({
      categories,
    });
  }

  handleDateChange(date) {
    this.setState({
      registrationDate: Moment(date),
    });
  }

  handleMapChange(point) {
    this.setState({ HQLocation: point });
  }

  render() {
    const {
      allCategories, org, formType, history,
    } = this.props;
    const {
      owner,
      categories, HQLocation,
      nameAr, nameEn, registrationCode, registrationDate,
      about, loading, logo, previewLogo,
    } = this.state;

    if (loading) return <Spin size="large" className="org__spinner" />;
    return (
      <div className="container">
        <Card
          title={formType === 'edit'
            ? `Edit ${nameEn} (${nameAr})`
            : 'Create new organization'
        }
          extra={<Button icon="arrow-left" title="Back" onClick={() => history.goBack()}>Back</Button>
              }
        >
          <div className="form__with-back" />
          <form className="orgs-form__form" onSubmit={this.handleSubmit}>
            <div className="flex column">
              <div className="flex row">
                <div className="org__basic__info-wrapper">
                  {/* names */}
                  <div className="flex row">
                    <div className="orgs-form__input">
                      <AntForm.Item label="Name in Arabic" required>
                        <Input name="nameAr" value={nameAr} onChange={this.handleChange} />
                      </AntForm.Item>
                    </div>
                    <div className="orgs-form__input">
                      <AntForm.Item label="Name in English" required>
                        <Input name="nameEn" value={nameEn} onChange={this.handleChange} />
                      </AntForm.Item>
                    </div>
                  </div>
                  {/* Registration date and code */}
                  <div className="flex row">
                    <div className="orgs-form__input">
                      <AntForm.Item label="Registration Code" required>
                        <Input
                          name="registrationCode"
                          value={registrationCode}
                          onChange={this.handleChange}
                        />
                      </AntForm.Item>
                    </div>
                    <div className="orgs-form__input">
                      <AntForm.Item label="Registration Date" required>
                        <DatePicker
                          disabledDate={date => Moment(date, dateFormat).isAfter(Moment()) || Moment(date, dateFormat).isBefore(Moment('1/1/1900', dateFormat))}
                          value={registrationDate}
                          name="registrationDate"
                          allowClear={false}
                          format={dateFormat}
                          onChange={this.handleDateChange}
                        />
                      </AntForm.Item>
                    </div>
                  </div>
                </div>
                <div className="org__logo">
                  <Avatar shape="square" src={previewLogo || logo} icon="picture" alt="organization logo" size={135} />
                  <div className="profile__upload-btn-wrapper">
                    <Button style={{ width: '13.5rem' }} icon="upload">
                      Logo
                    </Button>
                    <input type="file" name="logo" size="20" onChange={this.handleLogoChange} />
                  </div>
                </div>
              </div>
              {/* categories and about */}
              <div className="flex row">
                <div className="orgs-form__input">
                  <AntForm.Item label="Categories">
                    <Select
                      showSearch
                      allowClear
                      name="categories"
                      mode="multiple"
                      placeholder="Search and insert categories..."
                      value={categories}
                      onChange={this.handleCategoriesChange}
                    >
                      {allCategories.map(c => (<Select.Option key={c._id}>{c.name}</Select.Option>))}
                    </Select>
                  </AntForm.Item>
                </div>
                <div className="orgs-form__input">
                  <AntForm.Item label="About">
                    <Input.TextArea name="about" value={about} className="fill" onChange={this.handleChange} />
                  </AntForm.Item>
                </div>
              </div>
              <div className="orgs-form__input">
                <AntForm.Item label="The email of this organization owner" required>
                  <Input
                    prefix={<Icon type="mail" />}
                    placeholder="owner@example.com"
                    type="email"
                    name="owner"
                    value={owner}
                    onChange={this.handleChange}
                  />
                </AntForm.Item>
              </div>
              <div className="flex column">
                <div className="orgs-form__input-file">
                  <AntForm.Item label="Legal Files">
                    <Input
                      type="file"
                      multiple
                      onChange={this.handleFilesChange}
                    />
                  </AntForm.Item>
                </div>
                {org && <FilesDisplayer orgId={org._id} />}
              </div>
            </div>
            <hr style={{ color: 'gray', margin: '2em 0' }} />
            <AntForm.Item label="The organization headquarter address and location" required extra={<Icon type="pin" />}>
              <div className="orgs-form__map">
                <GoogleMap
                  point={HQLocation}
                  onChange={this.handleMapChange}
                />
              </div>
            </AntForm.Item>
            <div className="orgs-form__input">
              <br />
              {formType === 'edit'
                ? <Button block type="primary" title="Save" htmlType="submit">Save</Button>
                : <Button block type="primary" title="Create" htmlType="submit">Create</Button>
              }
            </div>
          </form>
        </Card>
      </div>
    );
  }
}

Form.propTypes = {
  allCategories: PropTypes.array.isRequired,
  formType: PropTypes.string.isRequired,
  history: PropTypes.instanceOf(Object).isRequired,
};

export default withRouter(Form);
