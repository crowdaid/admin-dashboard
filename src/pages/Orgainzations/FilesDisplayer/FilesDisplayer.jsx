import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { message, Modal, Icon } from 'antd';
import { CollectionsMeta } from '../../../api/CollectionsMeta';
import FilesCollection from '../../../api/Files';

const removeFile = (file) => {
  Modal.confirm({
    title: `Remove ${file.name}?`,
    content: 'This file will be removed permanently.',
    onOk: () => {
      Meteor.call('files.remove', { _id: file._id }, (err, res) => {
        if (err) {
          message.error(`something went wrong, ${err}`);
        } else {
          message.success('File removed successfully');
        }
      });
    },
    onCancel: () => {
    },
  });
};

const FileItem = ({ file, readOnly }) => (
  <li className="org__list__item">
    <div>{file.name}</div>
    <div className="orgs__list__item-actions">
      <a title={`open ${file.name}`} href={FilesCollection.findOne({ _id: file._id }).link()} rel="noopener noreferrer" target="_blank">
        <Icon type="eye" color="#00b894" />
      </a>
      {!readOnly
        && (
        <span title={`remove ${file.name}`}>
          <Icon type="delete" color="#d63031" onClick={() => removeFile(file)} />
        </span>
        )
        }
    </div>
  </li>
);

const Files = ({ files, total, readOnly }) => (
  <div className="org__files">
    {total
      ? (
        <ul className="org__files__list">
          <h4 style={{ color: 'gray' }}>{` Number of existing files: ${total}`}</h4>
          {files.map(file => (
            <FileItem key={file._id} file={file} readOnly={readOnly} />
          ))}
        </ul>
      )
      : <h4 style={{ color: 'gray' }}>No files for this organization yet.</h4> }
  </div>
);

FileItem.propTypes = {
  file: PropTypes.instanceOf(Object).isRequired,
};

Files.defaultProps = {
  readOnly: false,
};

Files.propTypes = {
  files: PropTypes.array.isRequired,
  total: PropTypes.number.isRequired,
  readOnly: PropTypes.bool,
};

export default withTracker((props) => {
  if (!props.orgId) {
    return {
      loading: true,
      files: [],
      total: 0,
    };
  }
  const filesHandle = Meteor.subscribe('files.legal.org', {
    filter: {},
    orgId: props.orgId,
  });
  if (!filesHandle.ready()) {
    return ({
      loading: true,
      files: [],
      total: 0,
    });
  }
  const filesMeta = CollectionsMeta.findOne({ _id: 'Files' });

  const files = FilesCollection.find({ 'meta.orgId': props.orgId }, {
    sort: { _id: 'DESC' },
  });
  const totalFiles = filesMeta && filesMeta.count;

  const filesExist = totalFiles > 0;

  return {
    loading: false,
    files: filesExist ? files.fetch() : [],
    total: totalFiles,
  };
})(Files);
