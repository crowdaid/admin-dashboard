import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import CategoriesCollection from '../../../api/Categories';
import Form from '../Form';

const Create = ({ allCategories, loading }) => {
  if (loading) return (<Spin size="large" />);
  return (<Form allCategories={allCategories} formType="create" />);
};

Create.propTypes = { allCategories: PropTypes.array.isRequired };

export default withTracker((props) => {
  const categoriesHandle = Meteor.subscribe('categories.all');

  if (!categoriesHandle.ready()) {
    return {
      loading: true,
      allCategories: [],
    };
  }

  const allCategories = CategoriesCollection.find({ categoryType: 'cause' }).fetch();

  return {
    loading: false,
    allCategories,
  };
})(Create);
