import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Modal, message, Button, Tag, Card, Icon,
} from 'antd';
import { CollectionsMeta } from '../../api/CollectionsMeta';
import OrganizationsCollection from '../../api/Organizations';
import { searchTables } from '../../components/tablesFilters';
import OrgAvarar from '../../components/OrgAvarar';

const PerPage = 10;
const PageNumber = new ReactiveVar(1);

const removeOrg = (org) => {
  Modal.confirm({
    iconType: 'cross',
    okText: 'Remove',
    okType: 'danger',
    title: `Remove Organization: ${org.name_en || org.name_ar}?`,
    content: `By removing this organization, All it's
     current saff and the owner will not be able to login to it.`,
    onOk() {
      Meteor.call('organizations.methods.remove', { _id: org.id }, (err, res) => {
        if (err) {
          message.error('something went wrong');
        } else {
          message.success('Organization removed successfully');
        }
      });
    },
    onCancel() {},
  });
};

class OrganizationsList extends Component {
  constructor(props) {
    super(props);
    this.state = { searchText: '' };
    // bind search tables with 'this' class
    this.getColumnSearchProps = searchTables.getColumnSearchProps.bind(this);
    this.handleSearch = searchTables.handleSearch.bind(this);
    this.handleReset = searchTables.handleReset.bind(this);
    // set the title of the page
    document.title = 'Organizations | Crowdaid';
  }

  render() {
    const {
      orgs, pagination, loading, history,
    } = this.props;

    const dataSource = orgs.map(org => ({
      id: org._id, name_ar: org.name.ar, name_en: org.name.en, key: org._id, owner: org.owner,
    }));

    const columns = [
      {
        title: 'Logo',
        key: 'logo',
        render: record => (
          <OrgAvarar shape="circle" size={50} orgId={record.id} />
        ),
      },
      {
        title: 'Arabic Name',
        dataIndex: 'name_ar',
        key: 'name_ar',
        ...this.getColumnSearchProps('name_ar', 'arabic names', this),
      },
      {
        title: 'English Name',
        dataIndex: 'name_en',
        key: 'name_en',
        ...this.getColumnSearchProps('name_en', 'english names', this),
      },
      {
        title: 'Owner',
        dataIndex: 'owner',
        key: 'onwer',
        ...this.getColumnSearchProps('owner', 'Owners emails', this),
      },
      {
        title: 'Actions',
        key: 'actions',
        render: (text, record) => {
          if (record.isRemoved) {
            return (<Tag color="danger"> removed </Tag>);
          }
          return (
            <span>
              <Button
                className="org__button"
                type="ghost"
                title="View"
                shape="circle-outline"
                icon="eye"
                onClick={() => history.push(`/organizations/${record.id}/view`)}
              />
              <Button
                className="org__button"
                type="primary"
                title="Edit"
                shape="circle-outline"
                icon="edit"
                onClick={() => history.push(`/organizations/${record.id}/edit`)}
              />
              <Button
                className="org__button"
                type="danger"
                title="Remove"
                shape="circle-outline"
                icon="delete"
                onClick={() => removeOrg(record)}
              />
            </span>
          );
        },
      },
    ];

    return (
      <div className="container">
        <Card
          bordered={false}
          title={(
            <h3>
              <Icon type="global" />
              {' '}
              Organizations
            </h3>
            )}
          extra={<Button type="primary" onClick={() => history.push('/organizations/add')} icon="plus"> Add New</Button>}
        >
          <Table
            pagination={pagination}
            loading={loading}
            dataSource={dataSource}
            columns={columns}
          />
        </Card>
      </div>
    );
  }
}

OrganizationsCollection.propTypes = {
  loading: PropTypes.bool.isRequired,
  orgs: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const orgsHandle = Meteor.subscribe('OrganizationsTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
  });
  if (!orgsHandle.ready()) {
    return ({
      loading: true,
      orgs: [],
      pagination: {},
    });
  }
  const orgsMeta = CollectionsMeta.findOne({ _id: 'Organizations' });

  const orgs = OrganizationsCollection.find({}, {
    sort: { createdAt: -1 },
  });

  const totalOrgs = orgsMeta && orgsMeta.count;

  const orgsExist = totalOrgs > 0;

  return {
    loading: false,
    orgs: orgsExist ? orgs.fetch() : [],
    total: totalOrgs,
    pagination: {
      position: 'bottom',
      total: totalOrgs,
      current: PageNumber.get(),
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(OrganizationsList);
