import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Spin } from 'antd';
import CategoriesCollection from '../../../api/Categories';
import OrganizationsCollection from '../../../api/Organizations';
import Files from '../../../api/Files';

import Form from '../Form';

const Edit = ({ allCategories, org, loading }) => {
  if (loading) return (<Spin size="large" />);
  return (<Form allCategories={allCategories} org={org} formType="edit" />);
};

Edit.propTypes = {
  org: PropTypes.instanceOf(Object).isRequired,
  allCategories: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
};

export default withTracker((props) => {
  const { match: { params: { id } }, history } = props;
  const categoriesHandle = Meteor.subscribe('categories.all');
  const orgHandle = Meteor.subscribe('organizations.single', id);
  const logoHandle = Meteor.subscribe('files.organizations.logo', id);

  if (!categoriesHandle.ready() || !orgHandle.ready() || !logoHandle.ready()) {
    return {
      allCategories: [],
      org: {},
      loading: true,
      logo: 'df',
    };
  }

  const org = OrganizationsCollection.findOne({ _id: id });

  if (!org) {
    history.replace('/404');
  }
  const logo = Files.findOne({ _id: org.logo, 'meta.type': 'logo' });

  const allCategories = CategoriesCollection.find({ categoryType: 'cause' }).fetch();
  org.logo = (logo && logo.link()) || '';

  return {
    loading: false,
    allCategories,
    org,
  };
})(Edit);
