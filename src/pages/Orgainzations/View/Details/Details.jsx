import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Card, Row, Col, Spin, Tag, Popover,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import CategoriesCollection from '../../../../api/Categories';
import GoogleMap from '../../../../components/GoogleMap';
import FilesDisplayer from '../../FilesDisplayer';


const ShowCategory = ({ category, categories }) => (
  <Popover
    content={(
      <div style={{ width: 400 }}>
        <Row>
          <Col span={24}>
            <Card style={{ textAlign: 'center' }}>
              {category.description || 'No Description'}
              <br />
              <small>
                <strong>Aliases:</strong>
                {category.aliases.length ? category.aliases.reduce((a, t) => `${a}, ${t}`) : ' No Aliases'}
              </small>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col span={10}>
            <Card><strong>Parent category</strong></Card>
          </Col>
          <Col span={14}>
            <Card style={{ textAlign: 'center' }}>{category.parentCategory ? categories.reduce((a, c) => c._id === category.parentCategory && c.name) || '-' : '-'}</Card>
          </Col>
        </Row>
      </div>
    )}
  >
    <Tag>{category.name}</Tag>
  </Popover>
);

const Details = (props) => {
  const { loading, org, allCategories } = props;

  if (loading) return <Spin />;
  return (
    <Row>
      <Row>
        <Col span={5}>
          <Card>
            <strong>Name AR</strong>
          </Card>
        </Col>
        <Col span={7}>
          <Card>
            {org.name.ar}
          </Card>
        </Col>
        <Col span={5}>
          <Card>
            <strong>Name EN</strong>
          </Card>
        </Col>
        <Col span={7}>
          <Card>
            {org.name.en}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}>
          <Card>
            <strong>Registration Date</strong>
          </Card>
        </Col>
        <Col span={7}>
          <Card>
            {org.registrationDate.toDateString()}
          </Card>
        </Col>
        <Col span={5}>
          <Card>
            <strong>Registration Code</strong>
          </Card>
        </Col>
        <Col span={7}>
          <Card>
            {org.registrationCode}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}>
          <Card>
            <strong>Owner Email</strong>
          </Card>
        </Col>
        <Col span={19}>
          <Card>
            {org.owner}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}>
          <Card>
            <strong>Categories</strong>
          </Card>
        </Col>
        <Col span={19}>
          <Card>
            {(org.categories && org.categories.length !== 0)
              ? allCategories.filter(t => org.categories.indexOf(t._id) !== -1).map(c => (
                <ShowCategory key={c._id} category={c} categories={allCategories} />))
              : <p>No categories yet</p>}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Card title={<strong>Files</strong>}>
            <FilesDisplayer orgId={org._id} readOnly />
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Card title={(
            <p>
              <strong>Location: </strong>
              {org.HQLocation.address}
            </p>)}
          >
            <GoogleMap
              readOnly
              point={org.HQLocation}
            />
          </Card>
        </Col>
      </Row>
    </Row>
  );
};

export default withTracker((props) => {
  const categoriesHandle = Meteor.subscribe('categories.all');

  if (!categoriesHandle.ready()) {
    return {
      loading: true,
      allCategories: [],
    };
  }

  const allCategories = CategoriesCollection.find({}).fetch();
  return {
    loading: false,
    allCategories,
  };
})(Details);
