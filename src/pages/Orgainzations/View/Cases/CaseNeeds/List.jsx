import React from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Table, Card } from 'antd';
import { CollectionsMeta } from '../../../../../api/CollectionsMeta';
import NeedsCollection from '../../../../../api/Needs';

const PerPage = 3;
const PageNumber = new ReactiveVar(1);

const CaseNeeds = ({ needs, pagination, loading }) => {
  const dataSource = needs.map(c => ({
    id: c._id,
    key: c._id,
    title: c.title,
    status: c.status,
    urgency: c.urgency,
    dueDate: (new Date(c.dueDate)).toDateString(),
  }));

  const columns = [
    {
      title: 'Title',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
    {
      title: 'Urgency',
      dataIndex: 'urgency',
      key: 'urgency',
    },
    {
      title: 'Due',
      dataIndex: 'dueDate',
      key: 'dueDate',
    },
  ];

  return (
    <Card title={<strong>Needs</strong>}>
      <Table
        pagination={pagination}
        loading={loading}
        dataSource={dataSource}
        columns={columns}
      />
    </Card>
  );
};

CaseNeeds.propTypes = {
  loading: PropTypes.bool.isRequired,
  needs: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const { id } = props;
  const needsHandle = Meteor.subscribe('cases.needs', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
    caseId: id,
  });
  if (!needsHandle.ready()) {
    return ({
      loading: true,
      needs: [],
      pagination: {},
    });
  }
  const needsMeta = CollectionsMeta.findOne({ _id: 'Needs' });

  const needs = NeedsCollection.find({ caseId: id }, {
    sort: { _id: 'DESC' },
  });

  const totalNeeds = needsMeta && needsMeta.count;

  const needsExist = totalNeeds > 0;

  return {
    loading: false,
    needs: needsExist ? needs.fetch() : [],
    caseId: id,
    total: totalNeeds,
    pagination: {
      position: 'bottom',
      total: totalNeeds,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(CaseNeeds);
