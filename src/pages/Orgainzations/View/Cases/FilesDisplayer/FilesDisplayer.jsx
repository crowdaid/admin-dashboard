import React from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Icon, Collapse,
} from 'antd';
import { CollectionsMeta } from '../../../../../api/CollectionsMeta';
import FilesCollection from '../../../../../api/Files';

const FileItem = ({ file }) => (
  <li className="files-displayer__list__item">
    <div>{file.name}</div>
    <div className="files-displayer__list__item-actions">
      <a title={`open ${file.name}`} href={FilesCollection.findOne({ _id: file._id }).link()} rel="noopener noreferrer" target="_blank">
        <Icon type="eye" color="#00b894" />
      </a>
    </div>
  </li>
);

const Files = ({ files, total }) => {
  if (total) {
    return (
      <Collapse bordered={false}>
        <Collapse.Panel
          header={(
            <p>
              <Icon type="file" />
              {'  '}
              View case attachments
            </p>
          )}
          key="1"
        >
          <div className="files-displayer__filesc">
            <ul className="files-displayer__files__list">
              <h4 style={{ color: 'gray' }}>{` Number of existing files: ${total}`}</h4>
              {files.map(file => (
                <FileItem key={file._id} file={file} />
              ))}
            </ul>
          </div>
        </Collapse.Panel>
      </Collapse>
    );
  }
  return <h4 style={{ color: 'gray' }}>No files for this case yet.</h4>;
};

FileItem.propTypes = {
  file: PropTypes.instanceOf(Object).isRequired,
};

Files.propTypes = {
  files: PropTypes.array.isRequired,
  total: PropTypes.number.isRequired,
};

export default withTracker((props) => {
  if (!props.caseId) {
    return {
      loading: true,
      files: [],
      total: 0,
    };
  }
  const filesHandle = Meteor.subscribe('files.cases.attachments', {
    filter: {},
    caseId: props.caseId,
  });
  if (!filesHandle.ready()) {
    return ({
      loading: true,
      files: [],
      total: 0,
    });
  }
  const filesMeta = CollectionsMeta.findOne({ _id: 'Files' });

  const files = FilesCollection.find({ 'meta.caseId': props.caseId }, {
    sort: { _id: 'DESC' },
  });
  const totalFiles = filesMeta && filesMeta.count;

  const filesExist = totalFiles > 0;

  return {
    loading: false,
    files: filesExist ? files.fetch() : [],
    total: totalFiles,
  };
})(Files);
