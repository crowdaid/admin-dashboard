import React from 'react';
import {
  Card, Col, Popover, Row, Spin, Tag,
} from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import CaseNeeds from '../CaseNeeds';
import FileDisplayer from '../FilesDisplayer';
import GoogleMap from '../../../../../components/GoogleMap';
import CategoriesCollection from '../../../../../api/Categories';
import Beneficiaries from '../../../../../api/Beneficiaries';
import NeedsCollection from '../../../../../api/Needs';

const ShowCategory = ({ category, categories }) => (
  <Popover
    content={(
      <div style={{ width: 400 }}>
        <Row>
          <Col span={24}>
            <Card style={{ textAlign: 'center' }}>
              {category.description || 'No Description'}
              <br />
              <small>
                <strong>Aliases:</strong>
                {category.aliases.length ? category.aliases.reduce((a, t) => `${a}, ${t}`) : ' No Aliases'}
              </small>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col span={10}>
            <Card><strong>Parent category</strong></Card>
          </Col>
          <Col span={14}>
            <Card style={{ textAlign: 'center' }}>{category.parentCategory ? categories.reduce((a, c) => c._id === category.parentCategory && c.name) || '-' : '-'}</Card>
          </Col>
        </Row>
      </div>
        )}
  >
    <Tag>{category.name}</Tag>
  </Popover>
);

const Case = ({
  loading, c, responsible, researcher, beneficiary, allCategories, totalNeeds,
}) => {
  if (loading) return <Spin />;
  return (
    <Row>
      <Row>
        <Col span={5}><Card><strong>Beneficiary</strong></Card></Col>
        <Col span={19}>
          <Card>
            {(beneficiary && beneficiary.name) ? beneficiary.name.ar : '-'}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}><Card><strong>Researcher</strong></Card></Col>
        <Col span={19}>
          <Card>
            {researcher ? researcher.profile.firstName : '-'}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}><Card><strong>Responsible</strong></Card></Col>
        <Col span={19}>
          <Card>
            {responsible ? responsible.profile.firstName : '-'}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}>
          <Card>
            <strong>Categories</strong>
          </Card>
        </Col>
        <Col span={19}>
          <Card>
            {allCategories.filter(t => c.categories.indexOf(t._id) !== -1).map(t => (
              <ShowCategory key={t._id} category={t} categories={allCategories} />))}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}><Card><strong>Urgency</strong></Card></Col>
        <Col span={19}>
          <Card>
            {c.urgency}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}><Card><strong>Due Date</strong></Card></Col>
        <Col span={19}>
          <Card>
            {c.dueDate.toDateString()}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}><Card><strong>Status</strong></Card></Col>
        <Col span={7}>
          <Card>
            {c.status}
          </Card>
        </Col>
        <Col span={5}><Card><strong>Total Price</strong></Card></Col>
        <Col span={7}>
          <Card>
            {`${totalNeeds} EGP`}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={5}><Card><strong>Address</strong></Card></Col>
        <Col span={19}>
          <Card>
            {c.caseLocation.address}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Card title={<strong>Description</strong>}>
            {c.description}
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Card title={<strong>Files</strong>}>
            <FileDisplayer caseId={c._id} />
          </Card>
        </Col>
      </Row>
      <br />
      <Row>
        <Col span={24}>
          <Card title={<strong>Location</strong>}>
            <GoogleMap
              readOnly
              point={c.caseLocation}
            />
          </Card>
        </Col>
      </Row>
      <br />
      <CaseNeeds id={c._id} />
    </Row>
  );
};

export default withTracker((props) => {
  const { c } = props;
  const usersHandle = Meteor.subscribe('cases.users', c);
  const categoriesHandle = Meteor.subscribe('categories.all');

  if (!usersHandle.ready() || !categoriesHandle.ready()) {
    return {
      loading: true,
      allCategories: [],
      totalNeeds: 0,
    };
  }
  Meteor.subscribe('beneficiaries.single', c.beneficiary);

  const responsible = Meteor.users.findOne({ _id: c.responsible });
  const beneficiary = Beneficiaries.findOne({ _id: c.beneficiary });
  const researcher = Meteor.users.findOne({ _id: c.researcher });
  const allCategories = CategoriesCollection.find({}).fetch();
  const needs = NeedsCollection.find({ caseId: c._id }).fetch();
  let totalNeeds = 0;
  needs.forEach((v, i) => {
    if (v.isQuantifiable) totalNeeds += v.itemPrice * v.quantityNeeded;
  });

  return {
    loading: false,
    researcher,
    beneficiary,
    responsible,
    allCategories,
    totalNeeds,
  };
})(Case);
