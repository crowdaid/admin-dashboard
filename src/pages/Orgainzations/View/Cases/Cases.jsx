import React from 'react';
import { withTracker } from 'meteor/react-meteor-data';
import {
  Row, Spin, Collapse,
} from 'antd';
import { Meteor } from 'meteor/meteor';
import CasesCollection from '../../../../api/Cases';
import Case from './Case';

const Cases = ({ loading, cases }) => {
  if (loading) return <Spin />;
  return (
    <Row>
      <Collapse bordered>
        { cases.length ? cases.map(c => (
          <Collapse.Panel header={c.title} key={c._id}>
            <Case c={c} />
          </Collapse.Panel>
        )) : <p>No Cases Available</p>}
      </Collapse>
    </Row>
  );
};

export default withTracker((props) => {
  const casesHandle = Meteor.subscribe('org.cases', props.org._id);

  if (!casesHandle.ready()) {
    return {
      loading: true,
      cases: [],
    };
  }

  const cases = CasesCollection.find({}).fetch();
  return {
    loading: false,
    cases,
  };
})(Cases);
