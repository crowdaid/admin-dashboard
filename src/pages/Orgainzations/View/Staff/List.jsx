import React from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import { Table, Card } from 'antd';
import { CollectionsMeta } from '../../../../api/CollectionsMeta';
import EmployeesCollection from '../../../../api/Employees';

const PerPage = 5;
const PageNumber = new ReactiveVar(1);

const StaffList = ({ employees, pagination, loading }) => {
  const dataSource = employees.map((user) => {
    if (!user.profile) user.profile = [];
    return {
      ...user, key: user._id, first_name: user.profile.firstName, last_name: user.profile.lastName, mobile: user.profile.mobile, identification: user.profile.idNumber,
    };
  });

  const columns = [
    {
      title: 'First Name',
      dataIndex: 'first_name',
      key: 'first_name',
    },
    {
      title: 'Last Name',
      dataIndex: 'last_name',
      key: 'last_name',
    },
    {
      title: 'Mobile',
      dataIndex: 'mobile',
      key: 'mobile',
    },
    {
      title: 'ID',
      dataIndex: 'identification',
      key: 'identification',
    },
    {
      title: 'Email Address',
      dataIndex: 'emails[0].address',
      key: 'email',
    },
  ];

  return (
    <Card>
      <Table
        pagination={pagination}
        loading={loading}
        dataSource={dataSource}
        columns={columns}
      />
    </Card>);
};

StaffList.propTypes = {
  loading: PropTypes.bool.isRequired,
  employees: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const { org: { _id } } = props;
  const employeesHandle = Meteor.subscribe('StaffTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
    orgId: _id,
  });
  if (!employeesHandle.ready()) {
    return ({
      loading: true,
      employees: [],
      pagination: {},
    });
  }
  const employeesMeta = CollectionsMeta.findOne({ _id: 'Employees' });

  const employees = EmployeesCollection.find({}, {
    sort: { _id: 'DESC' },
  });

  const totalEmployees = employeesMeta && employeesMeta.count;

  const EmployeesExist = totalEmployees > 0;
  return {
    loading: false,
    employeesReady: employeesHandle.ready(),
    employees: EmployeesExist ? employees.fetch() : [],
    total: totalEmployees,
    pagination: {
      position: 'bottom',
      total: totalEmployees,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(StaffList);
