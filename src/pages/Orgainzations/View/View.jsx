import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Modal, message, Button, Card, Spin, Tooltip,
} from 'antd';
import Details from './Details';
import Cases from './Cases';
import Beneficiaries from './Beneficiaries';
import Staff from './Staff';

import OrganizationsCollection from '../../../api/Organizations';
import OrgAvarar from '../../../components/OrgAvarar';


const tabList = [{
  key: 'details',
  tab: 'Details',
}, {
  key: 'cases',
  tab: 'Cases',
}, {
  key: 'beneficiaries',
  tab: 'Beneficiaries',
}, {
  key: 'staff',
  tab: 'Staff',
}];

const removeOrg = (org) => {
  Modal.confirm({
    title: `Remove Organization: ${org.name.en || org.name.ar}?`,
    content: 'This organization will be marked as removed.',
    onOk() {
      Meteor.call('organizations.methods.remove', { _id: org.id }, (err, res) => {
        if (err) {
          message.error('something went wrong');
        } else {
          message.success('Organization removed successfully');
        }
      });
    },
    onCancel() {},
  });
};

// upload cases and beneficiaries excel file
const uploadExcelFile = (event, org) => {
  const file = event.currentTarget.files[0];
  const reader = new FileReader();
  const rABS = !!reader.readAsBinaryString;
  reader.onload = (e) => {
    const data = e.target.result;
    /* Meteor magic */
    Meteor.call(rABS
      ? 'cases.methods.generateFromExcelS'
      : 'cases.methods.generateFromExcelU',
    {
      data: rABS ? data : new Uint8Array(data),
      name: file.name,
      orgId: org._id,
    }, (err, res) => {
      if (err) message.error(err.reason);
      else {
        message.success(`${res.insertedCount} case(s) inserted to ${org.name.ar}`);
      }
    });
  };
  if (rABS && file) reader.readAsBinaryString(file); else reader.readAsArrayBuffer(file);
};

const actionButtons = (org, history) => (
  <div>
    <Button icon="arrow-left" title="Back" onClick={() => history.goBack()}>Back</Button>
    <Button className="org__button" type="primary" icon="edit" onClick={() => history.replace(`/organizations/${org._id}/edit`)}>Edit</Button>
    <Button className="org__button" type="danger" icon="delete" onClick={() => removeOrg(org)}>Remove</Button>
    <div className="profile__upload-btn-wrapper" style={{ overflow: 'inherit' }}>
      <Tooltip title="Upload excel file with cases and beneficiaries to this organization">
        <Button
          style={{ width: '13.5rem', backgroundColor: '#10ac84', color: '#fff' }}
          icon="file-excel"
        >
          Upload excel
          <input type="file" accept=".xlsx" name="excelFile" size="1" style={{ width: 135, height: 66 }} onChange={e => uploadExcelFile(e, org)} />
        </Button>
      </Tooltip>
    </div>
  </div>
);

class ViewOrganization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: 'details',
    };

    document.title = 'Organizations | Crowdaid';
  }

    onTabChange = (key, type) => {
      this.setState({ [type]: key });
    }

    render() {
      const {
        org, loading, history, allCategories,
      } = this.props;
      const { key } = this.state;
      if (loading) return <Spin size="large" />;
      return (
        <div className="container">
          <Card
            style={{ width: '100%' }}
            title={(
              <div className="flex row">
                {org.logo && <OrgAvarar size={50} orgId={org._id} />}
                <h2 style={{ marginLeft: '1rem' }}>{`${org.name.en} (${org.name.ar})`}</h2>
              </div>)}
            extra={actionButtons(org, history)}
            tabList={tabList}
            activeTabKey={key}
            onTabChange={(key1) => { this.onTabChange(key1, 'key'); }}
          >
            {{
              details: <Details org={org} allCategories={allCategories || []} />,
              cases: <Cases org={org} />,
              beneficiaries: <Beneficiaries org={org} />,
              staff: <Staff org={org} />,
            }[key]}
          </Card>
        </div>
      );
    }
}

ViewOrganization.propTypes = {
  loading: PropTypes.bool.isRequired,
  org: PropTypes.instanceOf(Object).isRequired,
};

export default withTracker((props) => {
  const { match: { params: { id } } } = props;

  const orgHandle = Meteor.subscribe('organizations.single', id);
  if (!orgHandle.ready()) {
    return ({
      loading: true,
      org: {},
      allCategories: [],
    });
  }
  const org = OrganizationsCollection.findOne({ _id: id });

  return {
    loading: false,
    org,
  };
})(ViewOrganization);
