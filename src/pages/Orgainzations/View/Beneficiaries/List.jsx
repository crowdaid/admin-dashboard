import React from 'react';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { withTracker } from 'meteor/react-meteor-data';
import PropTypes from 'prop-types';
import {
  Table, Card, Row, Col,
} from 'antd';
import { CollectionsMeta } from '../../../../api/CollectionsMeta';
import BeneficiariesCollection from '../../../../api/Beneficiaries';

const PerPage = 5;
const PageNumber = new ReactiveVar(1);

const BenDetails = ({ record }) => (
  <Row>
    <Row>
      <Col span={5}><Card><strong>National ID</strong></Card></Col>
      <Col span={19}>
        <Card>
          {record.nationalIdNumber}
        </Card>
      </Col>
    </Row>
    <br />
    <Row>
      <Col span={5}><Card><strong>Birth Date</strong></Card></Col>
      <Col span={19}>
        <Card>
          {record.birthDate.toDateString()}
        </Card>
      </Col>
    </Row>
    <br />
    <Row>
      <Col span={5}><Card><strong>Address</strong></Card></Col>
      <Col span={19}>
        <Card>
          {record.address}
        </Card>
      </Col>
    </Row>
    <br />
    <Row>
      <Col span={24}>
        <Card title={<strong>About</strong>}>
          {record.about}
        </Card>
      </Col>
    </Row>
  </Row>
);

const List = ({ bens, pagination, loading }) => {
  const dataSource = bens.map(ben => ({
    id: ben._id,
    name_ar: ben.name.ar,
    name_en: ben.name.en,
    type: ben.type,
    key: ben._id,
    nationalIdNumber: ben.nationalIdNumber,
    birthDate: ben.birthDate,
    address: ben.address,
    about: ben.about,
    status: ben.verificationStatus,
  }));

  const columns = [
    {
      title: 'Arabic Name',
      dataIndex: 'name_ar',
      key: 'name_ar',
    },
    {
      title: 'English Name',
      dataIndex: 'name_en',
      key: 'name_en',
    },
    {
      title: 'Type',
      dataIndex: 'type',
      key: 'type',
    },
    {
      title: 'Status',
      dataIndex: 'status',
      key: 'status',
    },
  ];

  return (
    <Card>
      <Table
        pagination={pagination}
        loading={loading}
        expandedRowRender={record => <BenDetails record={record} />}
        dataSource={dataSource}
        columns={columns}
      />
    </Card>);
};

List.propTypes = {
  loading: PropTypes.bool.isRequired,
  bens: PropTypes.array.isRequired,
  pagination: PropTypes.object.isRequired,
};

export default withTracker((props) => {
  const { org } = props;
  const benHandle = Meteor.subscribe('BeneficiariesTabular', {
    filter: {},
    limit: PerPage,
    skip: (PageNumber.get() - 1) * PerPage,
    orgId: org._id,
  });
  if (!benHandle.ready()) {
    return ({
      loading: true,
      bens: [],
      pagination: {},
    });
  }
  const benMeta = CollectionsMeta.findOne({ _id: 'Beneficiaries' });

  const bens = BeneficiariesCollection.find({}, {
    sort: { _id: 'DESC' },
  });

  const totalBens = benMeta && benMeta.count;

  const bensExist = totalBens > 0;

  return {
    loading: false,
    bens: bensExist ? bens.fetch() : [],
    total: totalBens,
    pagination: {
      position: 'bottom',
      total: totalBens,
      defaultCurrent: 1,
      pageSize: PerPage,
      onChange(page, perPage) {
        PageNumber.set(page);
      },
    },
  };
})(List);
