/* eslint-disable import/no-extraneous-dependencies */
import React from 'react';
import { Layout, BackTop } from 'antd';
import { Redirect } from 'react-router-dom';
import ProtoTypes from 'prop-types';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import HeaderLayout from '../components/Header';

const { Content } = Layout;
/** Wrapper for Main routes with Navbar and main layout */
const MainLayout = ({ children, user }) => (
  <Layout style={{ minHeight: '100vh' }}>
    {!user && <Redirect to="/login" />}
    <Navbar />
    <Layout style={{ marginLeft: 200 }}>
      <HeaderLayout />
      <Content style={{ margin: '24px 16px 0', overflow: 'initial' }}>
        <div style={{ padding: 24, background: '#fff', minHeight: 360 }}>
          {children}
          <BackTop />
        </div>
        <Footer />
      </Content>
    </Layout>
  </Layout>
);
MainLayout.defaultProps = {
  user: undefined,
};

MainLayout.propTypes = {
  children: ProtoTypes.oneOfType([
    ProtoTypes.arrayOf(ProtoTypes.node),
    ProtoTypes.node,
  ]).isRequired,
  user: ProtoTypes.string,
};
// redirect to login if user logout in other tab
export default withTracker(props => ({ user: Meteor.userId() }))(MainLayout);
