import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import MainLayout from '../MainLayout';
import AuthRoute from '../AuthRoute';
import Admins from '../../pages/Admins';
import Owners from '../../pages/Owners';
import SignUp from '../../pages/SignUp';
import Login from '../../pages/Login';

import CreateOrganization from '../../pages/Orgainzations/Create';
import EditOrganization from '../../pages/Orgainzations/Edit';
import ViewOrganization from '../../pages/Orgainzations/View';
import OrganizationsList from '../../pages/Orgainzations';

import CategoriesList from '../../pages/Categories';
import CreateCategory from '../../pages/Categories/Create';
import EditCategory from '../../pages/Categories/Edit';

import Unallocated from '../../pages/UnallocatedDonations';
import EndUsers from '../../pages/EndUsers';

import Profile from '../../pages/Profile';
import VerifyEmail from '../../pages/Profile/VerifyEmail';
import Dashboard from '../../pages/Dashboard';

const App = props => (
  <main>
    <BrowserRouter>
      <Switch>
        {/* authentication routes  (without navbar and footer) */}
        <Route exact path="/create-account/:token" component={SignUp} />
        <Route exact path="/login" component={Login} />
        {/* Routes for the main layout (with navbar and footer) */}
        <MainLayout>
          <AuthRoute exact path="/" component={Dashboard} />
          <AuthRoute path="/admins" component={Admins} />
          <AuthRoute path="/owners" component={Owners} />
          <AuthRoute exact path="/organizations" component={OrganizationsList} />
          <AuthRoute exact path="/organizations/add" component={CreateOrganization} />
          <AuthRoute exact path="/organizations/:id/edit" component={EditOrganization} />
          <AuthRoute exact path="/organizations/:id/view" component={ViewOrganization} />

          <AuthRoute exact path="/categories/:id/edit" component={EditCategory} />
          <AuthRoute exact path="/categories/add" component={CreateCategory} />
          <AuthRoute exact path="/categories" component={CategoriesList} />


          <AuthRoute exact path="/unallocated" component={Unallocated} />

          <AuthRoute exact path="/verify-email/:token" component={VerifyEmail} />
          <AuthRoute exact path="/profile" component={Profile} />

          <AuthRoute exact path="/users" component={EndUsers} />
          <AuthRoute exact path="/users/:id" component={Profile} />
        </MainLayout>
      </Switch>
    </BrowserRouter>
  </main>
);

export default App;
