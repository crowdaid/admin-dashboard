import React, { Component, Fragment } from 'react';
import { Modal, Select, message } from 'antd';
import { Meteor } from 'meteor/meteor';
import { withTracker } from 'meteor/react-meteor-data';
import CategoriesCollection from '../../api/Categories';


class MergCats extends Component {
  constructor(props) {
    super(props);
    this.state = { selectedCat: null };
    this.handleMerge = this.handleMerge.bind(this);
    this.handleCategoryChange = this.handleCategoryChange.bind(this);
  }

  handleMerge() {
    const { handleCancel, parent } = this.props;
    const { selectedCat } = this.state;
    if (!parent || !selectedCat) {
      message.error(`Please select category to merge ${parent.name} with`);
      return;
    }

    Meteor.call('categories.methods.merge_cats', {
      _id: parent.id,
      mergedCatId: selectedCat,
    }, (error, result) => {
      if (error) {
        message.error(error.reason);
      } else {
        handleCancel();
        this.setState({ selectedCat: null });
        message.success('Category Merged successfully');
      }
    });
  }

  handleCategoryChange(selectedCat) {
    this.setState({ selectedCat });
  }

  render() {
    const {
      visible, handleCancel,
      categories, parent,
    } = this.props;
    const { selectedCat } = this.state;
    return (
      <div>
        <Modal
          title="Merge categories"
          visible={visible}
          okText="Merge"
          onOk={this.handleMerge}
          onCancel={handleCancel}
        >
          {categories.length !== 0
            ? (
              <Fragment>
                <p>{`Select category to merge with ${parent && parent.name} :`}</p>
                <Select
                  showSearch
                  style={{ width: 300 }}
                  className="fill"
                  value={selectedCat}
                  optionFilterProp="children"
                  onChange={this.handleCategoryChange}
                  filterOption={(input, option) => option.props.children
                    .toLowerCase().indexOf(input.toLowerCase()) >= 0}
                >
                  {categories.map(c => (
                    <Select.Option value={c._id} key={c._id}>
                      {c.name}
                    </Select.Option>
                  ))}
                </Select>
              </Fragment>
            )
            : <p>Their is no categories to merge with</p>
      }
        </Modal>
      </div>
    );
  }
}

export default withTracker(({ parent }) => {
  const categoriesHandle = Meteor.subscribe('categories.all');
  if (!categoriesHandle.ready()) {
    return {
      categories: [],
    };
  }
  const categories = parent && CategoriesCollection.find({
    _id: { $ne: parent.id },
    categoryType: parent.categoryType,
  }).fetch();
  return {
    categories: categories || [],
  };
})(MergCats);
