import React from 'react';
import { Layout, Menu, Icon } from 'antd';
import { Link, withRouter } from 'react-router-dom';

const { Sider } = Layout;
const { Item } = Menu;

const routes = [
  {
    key: 1, route: '/', title: 'Dashboard', icon: 'dashboard',
  },
  {
    key: 2, route: '/admins', title: 'Admins', icon: 'team',
  },
  {
    key: 3, route: '/users', title: 'Users', icon: 'user',
  },
  {
    key: 4, route: '/organizations', title: 'Organizations', icon: 'global',
  },
  {
    key: 5, route: '/owners', title: 'Owners', icon: 'crown',
  },
  {
    key: 6, route: '/categories', title: 'Categories', icon: 'tags',
  },
  {
    key: 7, route: '/unallocated', title: 'Unallocated donations', icon: 'appstore',
  },
];

const DetectCurrentRoute = (path) => {
  let selected = 1;

  routes.forEach((item, i) => {
    if (item.route === path || path.indexOf(`${item.route}/`) > -1) selected = item.key;
  });

  return selected;
};

const SideBar = (props) => {
  const { location: { pathname } } = props;
  const selectedKey = DetectCurrentRoute(pathname).toString();
  return (
    <Sider
      className="layout__sider"
      theme="dark"
    >
      <div className="layout__logo">
        <Link to="/">
          <img
            src="/images/logo.png"
            style={{ margin: '0 .5rem', borderRadius: '50%' }}
            width="35"
            height="35"
            alt="crowd aid"
          />
            crowd aid
        </Link>
      </div>
      <Menu
        mode="vertical"
        theme="dark"
        defaultSelectedKeys={[selectedKey]}
        style={{ height: '100%', borderRight: 0, backgroundColor: '#000' }}
      >
        {routes.map((item, i) => (
          <Item key={item.key}>
            <Link to={item.route}>
              <Icon type={item.icon} />
              {item.title}
            </Link>
          </Item>
        ))}
      </Menu>
    </Sider>
  );
};

export default withRouter(SideBar);
