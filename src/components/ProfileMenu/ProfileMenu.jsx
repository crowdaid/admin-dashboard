import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Icon, Avatar, Tooltip } from 'antd';
import { Link } from 'react-router-dom';
import { withTracker } from 'meteor/react-meteor-data';
import Files from '../../api/Files';

const capitalizeUserName = name => name.replace(/\b\w/g, l => l.toUpperCase());

const ProfileMenu = (props) => {
  const {
    avatar, user,
  } = props;
  return (
    <div>
      <div>
        <Link
          to="/profile"
          style={{
            color: 'white', textDecoration: 'none', marginLeft: 16, verticalAlign: 'middle',
          }}
        >
          {avatar
            ? <Avatar src={avatar} size="large" alt="avatar" />
            : (
              <Avatar
                size="large"
                style={{ backgroundColor: '#6c5ce7', fontSize: '2rem', verticalAlign: 'middle' }}
                alt="avatar"
              >
                {user && (user.profile.firstName ? user.profile.firstName[0].toUpperCase()
                  : 'A')}
              </Avatar>
            )}
          <span style={{ fontSize: '1.5rem', marginLeft: '1rem' }}>
            {capitalizeUserName(user && (user.profile.firstName
              ? user.profile.firstName : 'Admin'))}
          </span>
        </Link>
        <strong
          style={{
            cursor: 'pointer',
            color: 'white',
            marginLeft: 16,
            paddingLeft: 16,
            borderLeft: '1px solid white',
            fontSize: '2rem',
            verticalAlign: 'middle',
          }}
        >
          <Tooltip title="Logout">
            <Icon type="logout" size="large" onClick={e => Meteor.logout()} />
          </Tooltip>
        </strong>
      </div>
    </div>);
};


export default withTracker((props) => {
  // get the user id for the profile link if exists
  const { userId } = props;
  const userHandle = Meteor.subscribe('loggedUser', userId);
  const avatarHandle = Meteor.subscribe('files.avatar', userId);
  if (!userHandle.ready() && !avatarHandle.ready()) {
    return {
      user: '',
    };
  }
  // get user profile
  const user = Meteor.users.findOne({ _id: userId || Meteor.userId() }, { fields: { profile: 1 } });
  if (!(user && user.profile)) {
    return {
      user: '',
    };
  }

  // load the user avatar
  const avatar = Files.findOne({ _id: user.profile.avatar, 'meta.type': 'avatar' });
  return {
    ready: true,
    user: user || '',
    avatar: (avatar && avatar.link()) || '',
  };
})(ProfileMenu);
