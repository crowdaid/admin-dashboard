import React, { Component } from 'react';
import { captureException, withScope, showReportDialog } from '@sentry/browser';

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { error: null };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error });
    withScope((scope) => {
      Object.keys(errorInfo).forEach((key) => {
        scope.setExtra(key, errorInfo[key]);
      });
      captureException(error);
    });
  }

  render() {
    const { error } = this.state;
    // eslint-disable-next-line react/prop-types
    const { children } = this.props;
    if (error) {
      // render fallback UI
      return (
        <div style={{ textAlign: 'center' }}>
          <h1>
            Unfortunately the
            {' '}
            <b>crowd aid</b>
            {' '}
            app is crashed
            <span role="img" aria-labelledby="crashed app">💥</span>
          </h1>
          <p>
            {'جزمته ههه rabbit بيقولك مره ارنب اتكعبل عشان مكنش'}
          </p>
          <button
            style={{
              backgroundColor: '#e74c3c', color: '#fff', padding: 5, border: 'none',
            }}
            type="button"
            onClick={() => showReportDialog()}
          >
          Report on sentry
          </button>
          <a
            style={{
              backgroundColor: '#304D8A', color: '#fff', padding: 7, border: 'none', marginLeft: 2,
            }}
            href="/"
          >
              Back to Dashboard
          </a>
        </div>
      );
    }
    // when there's not an error, render children untouched
    return children;
  }
}
export default ErrorBoundary;
