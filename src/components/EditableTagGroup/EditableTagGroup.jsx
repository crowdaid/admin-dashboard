import React from 'react';
import {
  Tag, Input, Tooltip, Icon,
} from 'antd';
import PropTypes from 'prop-types';

class EditableTagGroup extends React.Component {
  constructor(props) {
    super(props);

    const { tags } = this.props;

    this.state = {
      tags,
      inputVisible: false,
      inputValue: '',
    };
  }

    showInput = () => {
      this.setState({ inputVisible: true }, () => this.input.focus());
    };

    handleClose = (removedTag) => {
      const { tags } = this.state;

      const filtered = tags.filter(tag => tag !== removedTag);

      const { handleTagsChanged } = this.props;

      handleTagsChanged(filtered);

      this.setState({ tags: filtered });
    };

    handleInputChange = (e) => {
      this.setState({ inputValue: e.target.value });
    };

    handleInputConfirm = () => {
      let { tags } = this.state;
      const { inputValue } = this.state;

      if (inputValue && tags.indexOf(inputValue) === -1) {
        tags = [...tags, inputValue];
      }
      const { handleTagsChanged } = this.props;

      handleTagsChanged(tags);

      this.setState({
        tags,
        inputVisible: false,
        inputValue: '',
      });
    };

    saveInputRef = (input) => {
      this.input = input;
      return input;
    };

    render() {
      const { placeholder } = this.props;
      const { tags, inputVisible, inputValue } = this.state;
      return (
        <div>
          {tags.map((tag, index) => {
            const isLongTag = tag.length > 20;
            const tagElem = (
              <Tag key={tag} closable afterClose={() => this.handleClose(tag)}>
                {isLongTag ? `${tag.slice(0, 20)}...` : tag}
              </Tag>
            );
            return isLongTag ? <Tooltip title={tag} key={tag}>{tagElem}</Tooltip> : tagElem;
          })}
          {inputVisible && (
            <Input
              ref={this.saveInputRef}
              type="text"
              size="small"
              style={{ width: 78 }}
              value={inputValue}
              onChange={this.handleInputChange}
              onBlur={this.handleInputConfirm}
              onPressEnter={this.handleInputConfirm}
            />
          )}
          {!inputVisible && (
            <Tag
              onClick={this.showInput}
              style={{ background: '#fff', borderStyle: 'dashed', marginTop: '5px' }}
            >
              <Icon type="plus" />
                {placeholder}
            </Tag>
          )}
        </div>
      );
    }
}

EditableTagGroup.defaultProps = {
  tags: [],
  placeholder: 'Add Tag',
};

EditableTagGroup.propTypes = {
  handleTagsChanged: PropTypes.func.isRequired,
  tags: PropTypes.array,
  placeholder: PropTypes.string,
};

export default EditableTagGroup;
