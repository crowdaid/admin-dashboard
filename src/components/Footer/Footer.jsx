import React from 'react';
import { Layout, Tag } from 'antd';
import { version } from '../../../package.json';

const Footer = props => (
  <Layout.Footer className="footer">
    <b>
      <Tag color="blue">
        Admin
        {' '}
      </Tag>
      Version
      <span>{version}</span>
      <p>
        crowd aid ©2019 created by Nezam.io
      </p>
    </b>
  </Layout.Footer>
);

export default Footer;
