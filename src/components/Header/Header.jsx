import React from 'react';
import { Layout } from 'antd';
import ProfileMenu from '../ProfileMenu/ProfileMenu';

const HeaderLayout = () => (
  <Layout.Header style={{ backgroundColor: '#000' }} className="header">
    <div className="layout__navbar--right">
      <ProfileMenu />
    </div>
  </Layout.Header>
);

export default HeaderLayout;
