/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component } from 'react';
import { Modal, Input, message } from 'antd';
import propTypes from 'prop-types';
import CustomIcons from '../CustomIcons/config.json';

class IconPicker extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false, selectedIcon: '', icons: CustomIcons.glyphs };
    this.handleOk = this.handleOk.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
  }

  componentWillReceiveProps({ visible }) {
    this.setState({ visible, selectedIcon: '' });
  }

  handleOk() {
    const { handlePick } = this.props;
    const { selectedIcon } = this.state;
    if (!selectedIcon) {
      message.error('Please pick an icon or cancel');
      return;
    }
    handlePick(selectedIcon);
  }

  handleCancel() {
    this.setState({ visible: false });
  }

  handleSearch(e) {
    const { value } = e.target;
    const filteredIcons = CustomIcons.glyphs.filter(icon => icon.css.includes(value));
    this.setState({ icons: filteredIcons });
  }

  render() {
    const { visible, selectedIcon, icons } = this.state;

    return (
      <div>
        <Modal
          destroyOnClose
          title={(
            <h2>

              Pick an icon
              {selectedIcon && (
              <i
                style={{ marginLeft: '1rem' }}
                className={`icon ion-md-${selectedIcon}`}
              />)}
            </h2>)}
          okText={(
            <div>

              Pick
              {selectedIcon && <i style={{ marginLeft: '1rem' }} className={`icon-${selectedIcon.css}`} />}
            </div>)}
          visible={visible}
          bodyStyle={{ height: '400px', overflowY: 'scroll' }}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <Input
            placeholder="Search icon..."
            allowClear
            onChange={this.handleSearch}
            style={{ width: '450px', height: '4rem', position: 'fixed' }}
          />
          <div className="iconPicker__container">
            {icons.map(icon => (
              <i
                role="img"
                key={icon.uid}
                onClick={() => this.setState({ selectedIcon: icon })}
                className={`icon-${icon.css} iconPicker__icon`}
              />
            ))}
          </div>

        </Modal>
      </div>
    );
  }
}
IconPicker.defaultProps = {
  visible: false,
};

IconPicker.propTypes = {
  handlePick: propTypes.func.isRequired,
  visible: propTypes.bool,
};

export default IconPicker;
