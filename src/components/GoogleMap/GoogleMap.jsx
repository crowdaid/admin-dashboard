import React from 'react';
import PropTypes from 'prop-types';
import GoogleMapReact from 'google-map-react';
import { Icon } from 'antd';

const Marker = ({ lat, lng }) => (
  <div className="google-map__marker" lat={lat} lng={lng}>
    <Icon theme="filled" type="environment" style={{ fontSize: '1em', position: 'absolute', bottom: 0 }} />
  </div>
);
const MAPOPTIONS = maps => ({
  mapTypeId: maps.MapTypeId.ROADMAP,
});

class GoogleMap extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: '',
      center: props.point && props.point.location.coordinates,
      zoom: 11,
      draggable: true,
      lat: props.point && props.point.location.coordinates[0],
      lng: props.point && props.point.location.coordinates[1],
      address: props.point && props.point.address,
      readOnly: props.readOnly,
    };
    this.searchBoxRef = React.createRef();
    this.handleChange = this.handleChange.bind(this);
    this.updateAddress = this.updateAddress.bind(this);
    // eslint-disable-next-line no-underscore-dangle
    this.onPositionChange = this.onPositionChange.bind(this);
    this.onCircleInteraction = this.onCircleInteraction.bind(this);
    this.handleOnClick = this.handleOnClick.bind(this);
    this.getPositionPoint = this.getPositionPoint.bind(this);
    this.onCircleInteraction = this.onCircleInteraction.bind(this);
    this.handleAddressChange = this.handleAddressChange.bind(this);
  }

  componentWillReceiveProps(newProps) {
    const { point } = newProps;
    if (point && point.address) {
      const { address, location: { coordinates } } = point;
      const lat = coordinates[0] || 30.0444;
      const lng = coordinates[1] || 31.2357;
      this.setState({
        address, center: [lat, lng], lat, lng,
      });
    } else {
      this.setState({ center: [30.0444, 31.2357] });
    }
  }

  onCircleInteraction(childKey, childProps, mouse) {
    this.setState({
      draggable: false,
      lat: mouse.lat,
      lng: mouse.lng,
    });
  }

  getPositionPoint(childKey, childProps, mouse) {
    this.setState({ draggable: true });
    this.updateAddress();
  }

  onPositionChange = ({ center, zoom }) => {
    this.setState({
      zoom,
    });
  };


  handleChange = () => {
    const { onChange } = this.props;
    const {
      address, lat, lng,
    } = this.state;
    const point = {
      address,
      location: {
        type: 'Point',
        coordinates: [lat, lng],
      },
    };

    // Update the parent
    onChange(point);
  };

  handleAddressChange() {
    const address = this.searchBoxRef.current.value;
    this.setState({ address });
  }

  handleSearch(map, maps) {
    this.setState({
      maps,
    });

    const searchBox = new maps.places.SearchBox(this.searchBoxRef.current);
    map.addListener('bounds_changed', () => {
      searchBox.setBounds(map.getBounds());
    });
    searchBox.addListener('places_changed', () => {
      const places = searchBox.getPlaces();
      if (places.length === 0) {
        return;
      }

      const place = places[0];

      if (!place || !place.geometry) {
        this.setState({ error: 'Returned place contains no geometry' });
        return;
      }
      const lat = place.geometry.location.lat();
      const lng = place.geometry.location.lng();
      this.setState({
        address: place.formatted_address, lat, lng, center: [lat, lng],
      });

      // change the parent point also on search
      this.handleChange();
    });
  }

  handleOnClick(mouse) {
    this.setState({
      lat: mouse.lat,
      lng: mouse.lng,
    });

    this.updateAddress();
  }

  updateAddress() {
    const { maps, lat, lng } = this.state;

    const geocoder = new maps.Geocoder();

    geocoder.geocode({ location: { lat, lng } }, (results, status) => {
      if (status === 'OK' && results[0]) {
        this.setState({
          address: results[0].formatted_address,
        });
        this.handleChange();
      } else {
        this.setState({
          address: 'Unnamed Location',
        });
      }
    });
  }

  render() {
    const {
      lat, lng, draggable, zoom, center, address, error, readOnly,
    } = this.state;
    if (!center) return <div>loading...</div>;
    if (error) return <div>{`Something went wrong, ${error}`}</div>;
    return (
      <div style={{ height: '70vh', width: '100%' }}>
        {!readOnly
          && (
          <div>
            <input
              id="searchBox"
              className="google-map__search-box"
              onChange={this.handleAddressChange}
              value={address}
              onKeyPress={(e) => { if (e.key === 'Enter') e.preventDefault(); }}
              placeholder="Search for place..."
              ref={this.searchBoxRef}
            />
            <p className="google-map__note">
              NOTE: you should update your address, If
              the location address you choose is appear as
              <b> Unnamed Road .</b>
            </p>
          </div>
          )}
        <GoogleMapReact
          bootstrapURLKeys={{
            libraries: 'places',
            key: 'AIzaSyDKcGgshQbWd1dv2loiM0nk6z9oqxx5tJY',
          }}
          options={MAPOPTIONS}
          draggable={draggable}
          onChange={this.onPositionChange}
          center={center}
          zoom={zoom}
          onClick={readOnly ? () => {} : this.handleOnClick}
          onChildMouseDown={readOnly ? () => {} : this.onCircleInteraction}
          onChildMouseUp={readOnly ? () => {} : this.getPositionPoint}
          onChildMouseMove={readOnly ? () => {} : this.onCircleInteraction}
          onGoogleApiLoaded={({ map, maps }) => { this.handleSearch(map, maps); }}
          yesIWantToUseGoogleMapApiInternals
          defaultCenter={[30.0444, 31.2357]}
        >
          <Marker lat={lat} lng={lng} />

        </GoogleMapReact>
      </div>
    );
  }
}

GoogleMap.defaultProps = {
  onChange: () => {},
  point: null,
  readOnly: false,
};

GoogleMap.propTypes = {
  onChange: PropTypes.func,
  point: PropTypes.object,
  readOnly: PropTypes.bool,
};
export default GoogleMap;
