import React from 'react';
import { Avatar } from 'antd';
import { withTracker } from 'meteor/react-meteor-data';
import { Meteor } from 'meteor/meteor';
import OrganizationsCollection from '../../api/Organizations';
import Files from '../../api/Files';

const OrgAvatar = ({ logo, shape, size }) => (
  <Avatar
    shape={shape || 'square'}
    src={logo}
    icon="picture"
    alt="organization logo"
    size={size || 'default'}
  />
);

export default withTracker(({ orgId }) => {
  const logoHandle = Meteor.subscribe('files.organizations.logo', orgId);

  if (!logoHandle.ready()) {
    return {
      org: {},
      loading: true,
      logo: 'df',
    };
  }

  const org = OrganizationsCollection.findOne({ _id: orgId });

  const logo = Files.findOne({ _id: org.logo, 'meta.type': 'logo' });

  return {
    logo: (logo && logo.link()) || '',
  };
})(OrgAvatar);
