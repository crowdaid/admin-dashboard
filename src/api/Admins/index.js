import { Mongo } from 'meteor/mongo';

const Admins = new Mongo.Collection('Admins');

export default Admins;
