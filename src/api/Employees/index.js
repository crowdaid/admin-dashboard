import { Mongo } from 'meteor/mongo';

const EmployeesCollection = new Mongo.Collection('employees');

export default EmployeesCollection;
