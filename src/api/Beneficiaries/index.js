import { Mongo } from 'meteor/mongo';

const BeneficiariesCollection = new Mongo.Collection('beneficiaries');

export default BeneficiariesCollection;
