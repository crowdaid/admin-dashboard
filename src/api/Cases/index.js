import { Mongo } from 'meteor/mongo';

const CasesCollection = new Mongo.Collection('cases');

export default CasesCollection;
