import { Mongo } from 'meteor/mongo';

const Owners = new Mongo.Collection('Owners');

export default Owners;
